package com.CPS240Final.Plugin.GUIs;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Disease.Disease;
import com.CPS240Final.Plugin.Util.Util;

/**
 * A GUI that lists the players current diseases
 * @author Johnny Leek
 *
 */
public class DiseaseGUI extends GUI{

	//The contents of the GUI
	List<ItemStack> guiContents = new ArrayList<ItemStack>();

	//A list of all diseases
	String[] diseases;

	/**
	 * Initializes the GUI
	 * @param title - the title of the GUI
	 * @param size - the size of the GUI
	 * @param diseases - the diseases
	 */
	public DiseaseGUI(String title, int size, String diseases) {
		super(title, size);

		this.diseases = diseases.split(",");

		if(diseases == "NONE") {
			this.diseases = new String[] {"NONE"};
		}

		initializeItems();

		for(int i = 0; i < guiContents.size(); i++) {
			super.addItem(guiContents.get(i), i);
		}
	}

	/**
	 * Initializes the items in the GUI
	 */
	private void initializeItems() {

		if(diseases[0].equals("NONE")) {
			guiContents.add(
					Util.createItem(Material.BARRIER,
									ChatColor.RED + "None",
									ChatColor.GREEN + "You don't have any diseases!"
							)
					);
			return;
		}

		for(String string : diseases) {

			for(Disease disease : Main.getInstance().getPossibleDiseases()) {
				if(string.equalsIgnoreCase(disease.getName())) {
					guiContents.add(disease.getIcon());
				}
			}

		}

	}

	/**
	 * When GUI is clicked
	 */
	@Override
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		try {
			if(e.getInventory().getHolder().getClass() != this.getInventory().getHolder().getClass()) return;

			e.setCancelled(true);
		} catch(NullPointerException ex) {
			return;
		}

	}



}
