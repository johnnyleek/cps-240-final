package com.CPS240Final.Plugin.GUIs;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

/**
 * An abstract class representing a custom chest GUI
 * @author Johnny Leek
 *
 */
public abstract class GUI implements InventoryHolder, Listener {

	private String title;
	private int size;
	private final Inventory gui;
	
	public GUI(String title, int size) {
		this.title = title;
		this.size = size;
		gui = Bukkit.createInventory(this, size, title);
	}
	
	/**
	 * Opens the custom inventory for whichever player requested it
	 * @param entity - The player entity that requested to view the GUI
	 */
	public void openInventory(HumanEntity entity) {
		entity.openInventory(gui);
	}
	
	/**
	 * Sets the inventory slot at the provided slot to the provided item
	 * @param item - The item to add
	 * @param slot - The slot to place the item
	 */
	protected void addItem(ItemStack item, int slot) {
		gui.setItem(slot, item);
	}
	
	/**
	 * An EventHandler to control what happens when the user interacts
	 * with the GUI
	 * @param e - The event
	 */
	@EventHandler
	public abstract void onInventoryClick(InventoryClickEvent e);
	
	/* Getters and Setters*/
	
	@Override
	public Inventory getInventory() {
		return gui;
	}

	public String getTitle() {
		return title;
	}


	public int getSize() {
		return size;
	}
	
}
