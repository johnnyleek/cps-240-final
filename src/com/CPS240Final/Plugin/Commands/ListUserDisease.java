package com.CPS240Final.Plugin.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.GUIs.DiseaseGUI;

/**
 * Command to open a GUI which lists the users current diseases
 * @author Johnny Leek
 *
 */
public class ListUserDisease implements CommandExecutor {

	/**
	 * Gets the command with the provided executor in "Main" and lists user diseases
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(sender instanceof Player) {
			Player player = (Player) sender;

			String diseaseList = (String)Main.getInstance().getDatabase("users").getPlayerData(player, "diseases");

			if(diseaseList == null || diseaseList == "") {
				DiseaseGUI gui = new DiseaseGUI(player.getName() + "'s Diseases", 9, "NONE");
				gui.openInventory(player);
			} else {
				DiseaseGUI gui = new DiseaseGUI(player.getName() + "'s Diseases", 9, diseaseList);
				gui.openInventory(player);
			}



		}
		return true;

	}

}
