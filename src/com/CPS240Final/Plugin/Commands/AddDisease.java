package com.CPS240Final.Plugin.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Disease.Disease;
import com.CPS240Final.Plugin.Disease.DiseaseCause;
import com.CPS240Final.Plugin.Util.Util;

/**
 * Command to add a disease to the user (using a specified name)
 * @author Johnny Leek
 *
 */
public class AddDisease implements CommandExecutor {
	
	/**
	 * Gets the command with the provided executor in "Main" and adds disease to user
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			Player player = (Player) sender;
			
			Disease diseaseToAdd = null;
			
			if(args.length == 0) {
				sender.sendMessage(Util.DISEASE_PREFIX + "Please specify a disease. Usage: /adddisease <NAME>");
				return true;
			}
			
			//Find disease
			for(Disease disease : Main.getInstance().getPossibleDiseases()) {
				if(disease.getCmdName().equalsIgnoreCase(args[0])) {
					diseaseToAdd = disease;
					break;
				}
			}
			
			if(diseaseToAdd == null) {
				sender.sendMessage(Util.DISEASE_PREFIX + "Disease not found!");
				return true;
			} else {
				Util.addDisease(player, diseaseToAdd, DiseaseCause.COMMAND);
			}
		}
		
		return true;
		
		
	}
	
}
