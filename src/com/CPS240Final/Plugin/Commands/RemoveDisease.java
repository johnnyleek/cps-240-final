package com.CPS240Final.Plugin.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Disease.Disease;
import com.CPS240Final.Plugin.Util.Util;

/**
 * Command to open a remove a disease from the user (using specified name)
 * @author Johnny Leek
 *
 */
public class RemoveDisease implements CommandExecutor {

	/**
	 * Gets the command with the provided executor in "Main" and removes diseases from user
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			Player player = (Player) sender;

			Disease diseaseToRemove = null;
			
			if(args.length == 0) {
				sender.sendMessage(Util.DISEASE_PREFIX + "Please specify a disease. Usage: /removedisease <NAME>");
				return true;
			}
			
			//Find disease
			for(Disease disease : Main.getInstance().getPossibleDiseases()) {
				if(disease.getCmdName().equalsIgnoreCase(args[0])) {
					diseaseToRemove = disease;
					break;
				}
			}
			
			if(diseaseToRemove == null) {
				sender.sendMessage(Util.DISEASE_PREFIX + "Disease not found!");
				return true;
			} else {
				Util.removeDisease(player, diseaseToRemove);
			}
		}
		
		return true;
		
		
	}
	
}
