package com.CPS240Final.Plugin.Encumbrance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Util.Util;

import net.md_5.bungee.api.ChatColor;

/**
 * Adds a carryweight for every item based on the items crafting recipe
 * and displays at the top of the screen using a dynamic bossbar
 * @author Johnny Leek
 *
 */
public class CarryWeight implements Listener{

	/**  Multipliers for each base material  **/
	private double COAL_MULTIPLIER;
	private double IRON_MULTIPLIER;
	private double GOLD_MULTIPLIER;
	private double DIAMOND_MULTIPLIER;
	private double EMERALD_MULTIPLIER;
	private double STONE_MULTIPLIER;
	private double WOOD_MULTIPLIER;
	
	//Maps material to the multiplier
	private Map<Material, Double> materialMultipliers;
	
	//Items default carry weight
	private int ITEM_DEFAULT_WEIGHT;
	
	//Bossbars for each player
	private static HashMap<UUID, BossBar> playerBossbars = new HashMap<UUID, BossBar>();
	
	/**
	 * Initialize Carryweight
	 */
	@SuppressWarnings("serial")
	public CarryWeight() {
		
		//Get multipliers from the configuration
		this.COAL_MULTIPLIER = Main.getInstance().getConfig().getDouble("CarryWeight.Multipliers.Coal");
		this.IRON_MULTIPLIER = Main.getInstance().getConfig().getDouble("CarryWeight.Multipliers.Iron");
		this.GOLD_MULTIPLIER = Main.getInstance().getConfig().getDouble("CarryWeight.Multipliers.Gold");
		this.DIAMOND_MULTIPLIER = Main.getInstance().getConfig().getDouble("CarryWeight.Multipliers.Diamond");
		this.EMERALD_MULTIPLIER = Main.getInstance().getConfig().getDouble("CarryWeight.Multipliers.Emerald");
		this.STONE_MULTIPLIER = Main.getInstance().getConfig().getDouble("CarryWeight.Multipliers.Stone");
		this.WOOD_MULTIPLIER = Main.getInstance().getConfig().getDouble("CarryWeight.Multipliers.Wood");
		
		//Get default weight from the configuration
		this.ITEM_DEFAULT_WEIGHT = Main.getInstance().getConfig().getInt("CarryWeight.DefaultWeight");
		
		//Initilize multiplier map
		materialMultipliers = new HashMap<Material, Double>() {
			{	
				put(Material.COAL, COAL_MULTIPLIER);
				put(Material.IRON_INGOT, IRON_MULTIPLIER);
				put(Material.IRON_BLOCK, (IRON_MULTIPLIER * 9));
				put(Material.GOLD_INGOT, GOLD_MULTIPLIER);
				put(Material.GOLD_NUGGET, (GOLD_MULTIPLIER / 9));
				put(Material.GOLD_BLOCK, (GOLD_MULTIPLIER * 9));
				put(Material.DIAMOND, DIAMOND_MULTIPLIER);
				put(Material.DIAMOND_BLOCK, (DIAMOND_MULTIPLIER * 9));
				put(Material.EMERALD, EMERALD_MULTIPLIER);
				put(Material.COBBLESTONE, STONE_MULTIPLIER);
				put(Material.ACACIA_PLANKS, WOOD_MULTIPLIER);
				put(Material.BIRCH_PLANKS, WOOD_MULTIPLIER);
				put(Material.DARK_OAK_PLANKS, WOOD_MULTIPLIER);
				put(Material.JUNGLE_PLANKS, WOOD_MULTIPLIER);
				put(Material.OAK_PLANKS, WOOD_MULTIPLIER);
				put(Material.SPRUCE_PLANKS, WOOD_MULTIPLIER);
			}
		};
	}
	
	/**
	 * When the user picks up an item, update the carry weight
	 * @param e - the event
	 */
	@EventHandler
	public void playerPickupItem(EntityPickupItemEvent e) {
		
		if(e.getEntity() instanceof Player) {
			Player player = (Player)e.getEntity();
			ItemStack item = e.getItem().getItemStack();

			int currentWeight = Main.getInstance().getDatabase("users").getPlayerData(player, "current_carryweight");
			int newWeight = currentWeight + calculateCarryWeight(item);
			
			Main.getInstance().getDatabase("users").setPlayerData(player, new String[] {"current_carryweight"}, new Integer[] {newWeight});
			
			handleEncumbered(player);
			
		}
		
		
	}
	
	/**
	 * When the user clicks in an inventory
	 * @param e - the event
	 */
	@EventHandler
	public void inventoryClick(InventoryClickEvent e) {
		try {
			//If the user is in an inventory (weird spigot thing)
			if(e.getInventory().getType() != null) {
				
				//The current item that the user is holding
				ItemStack currentItem = e.getCurrentItem();
				
				//If the user who clicked is a player (another weird spigot thing)
				if(e.getWhoClicked() instanceof Player) {
					//Player has placed item in inventory		
					Player player = (Player)e.getWhoClicked();
					int currentWeight = Main.getInstance().getDatabase("users").getPlayerData(player, "current_carryweight");
					int newWeight = currentWeight;
					if(currentItem.getType().equals(Material.AIR)) {
						//Player added item
						newWeight = currentWeight + calculateCarryWeight(e.getCursor());
					}
					
					else if(!currentItem.getType().equals(Material.AIR) && e.getCursor().getType().equals(Material.AIR)) {
						//Player took item
						newWeight = currentWeight - calculateCarryWeight(currentItem);
						if(newWeight < 0) newWeight = 0;
	
					}
					
					else if(!currentItem.getType().equals(Material.AIR) && !e.getCursor().getType().equals(Material.AIR)) {
						//Player swapped item
						newWeight = currentWeight - calculateCarryWeight(currentItem);
						newWeight += calculateCarryWeight(e.getCursor());
						if(newWeight < 0) newWeight = 0;	
					}
					
					Main.getInstance().getDatabase("users").setPlayerData(player, new String[] {"current_carryweight"}, new Integer[] {newWeight});
					
					handleEncumbered(player);
					
				}
			} 
			
		} catch(NullPointerException ex) {
			return;
		}
		
	}
	
	/**
	 * If the player drops an item, update carry weight
	 * @param e - the event
	 */
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		//If player is not in their inventory (the carry weight for that is handled in inventoryClick event above)
		if(!((Integer)Main.getInstance().getDatabase("users").getPlayerData(e.getPlayer(), "in_inventory") == 1)) {
			ItemStack item = e.getItemDrop().getItemStack();
			Player player = (Player)e.getPlayer();
			int currentWeight = Main.getInstance().getDatabase("users").getPlayerData(player, "current_carryweight");
			int newWeight = currentWeight - calculateCarryWeight(item);
			if(newWeight < 0) newWeight = 0;
			Main.getInstance().getDatabase("users").setPlayerData(player, new String[] {"current_carryweight"}, new Integer[] {newWeight});
			handleEncumbered(player);
		}
		
	}
	
	/**
	 * When the player opens inventory (update flag in user DB)
	 * @param e - the event
	 */
	@EventHandler
	public void onPlayerInventoryOpen(InventoryOpenEvent e) {
		Main.getInstance().getDatabase("users").setPlayerData((Player)e.getPlayer(), new String[] {"in_inventory"}, new Integer[] {1});
	}
	
	/**
	 * When the player closes inventory (update flag in user DB)
	 * @param e - the event
	 */
	@EventHandler
	public void onPlayerInventoryClose(InventoryCloseEvent e) {
		Main.getInstance().getDatabase("users").setPlayerData((Player)e.getPlayer(), new String[] {"in_inventory"}, new Integer[] {0});
	}
	
	/**
	 * When the player leaves the server (update inventory flag in DB)
	 * @param e - the event
	 */
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		Main.getInstance().getDatabase("users").setPlayerData((Player)e.getPlayer(), new String[] {"in_inventory"}, new Integer[] {0});
	}
	
	/**
	 * When player dies, set carry weight to 0 (they drop all items)
	 * @param e - the event
	 */
	@EventHandler
	public void onPlayerDeath(EntityDeathEvent e) {
	
		if(e.getEntity() instanceof Player) {
			Main.getInstance().getDatabase("users").setPlayerData((Player)e.getEntity(), new String[] {"current_carryweight"}, new Integer[] {0});
			handleEncumbered((Player)e.getEntity());
		}
		
	}
	
	/**
	 * Calcalate the cary weight for a given item
	 * @param item - the item to calculate weight for
	 * @return the carry weight
	 */
	private int calculateCarryWeight(ItemStack item) {
		//Get all the recipes for the item
		List<Recipe> craftingRecipes = Bukkit.getServer().getRecipesFor(item);
		int itemCarryWeight = ITEM_DEFAULT_WEIGHT;
		double multiplier = 1;
		if(craftingRecipes.size() > 0) {
			
			Recipe recipeToCheck = craftingRecipes.get(0);
			//Calculate multiplier based upon the items materials
			if(recipeToCheck instanceof ShapedRecipe) {
				ShapedRecipe shaped = (ShapedRecipe)recipeToCheck;
				List<ItemStack> shapedIngredients = new ArrayList<ItemStack>();
				shaped.getIngredientMap().forEach((character, recipeItem) -> shapedIngredients.add(recipeItem));
				multiplier = calculateCarryMultiplier(shapedIngredients);
			}
			
			else if(recipeToCheck instanceof ShapelessRecipe) {
				ShapelessRecipe shapeless = (ShapelessRecipe)recipeToCheck;
				multiplier = calculateCarryMultiplier(shapeless.getIngredientList());
			}
		
		}
		//Return the item carry weight * the items in the item stack;
		return (int)((itemCarryWeight * multiplier) * item.getAmount());
	}
	
	/**
	 * Handles if the player is encumbered or no longer encumbered
	 * @param player - the player to check
	 */
	public static void handleEncumbered(Player player) {
		int currentWeight = Main.getInstance().getDatabase("users").getPlayerData(player, "current_carryweight");
		int maxWeight = Main.getInstance().getDatabase("users").getPlayerData(player, "max_carryweight");
		
		//Get the bossbar for the current player
		BossBar carryweightBar = playerBossbars.get(player.getUniqueId());
		if(carryweightBar == null) {
			
			carryweightBar = Bukkit.createBossBar(String.format("Carry Weight: %d/%d", currentWeight, maxWeight), BarColor.GREEN, BarStyle.SEGMENTED_10);
			carryweightBar.addPlayer(player);
			playerBossbars.put(player.getUniqueId(), carryweightBar);
			
		}
		
		//Sets the title of the bar
		carryweightBar.setTitle(String.format(ChatColor.AQUA.toString() + ChatColor.BOLD + "Carry Weight: " + ChatColor.YELLOW.toString() + ChatColor.BOLD + "%d/%d", currentWeight, maxWeight));
		double carryPercentage = ((double)currentWeight / (double)maxWeight);
		
		//Gets the percentage of the bar
		if(carryPercentage >= 0.9) {
			carryweightBar.setColor(BarColor.PURPLE);
		}
		else if(carryPercentage >= 0.6) {
			carryweightBar.setColor(BarColor.RED);
		}
		else if(carryPercentage >= 0.3) {
			carryweightBar.setColor(BarColor.YELLOW);
		}
		else {
			carryweightBar.setColor(BarColor.GREEN);
		}
		
		if(carryPercentage > 1) carryPercentage = 1;
		carryweightBar.setProgress(carryPercentage);
		
		playerBossbars.put(player.getUniqueId(), carryweightBar);
		
		boolean isEncumbered = ((Integer)Main.getInstance().getDatabase("users").getPlayerData(player, "is_encumbered") == 1);
		
		if(currentWeight >= maxWeight) {
			Main.getInstance().getDatabase("users").setPlayerData(player, new String[] {"is_encumbered"}, new Integer[] {1});
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000000, 1));
			if(!isEncumbered)
				player.sendMessage(Util.CARRY_WEIGHT_PREFIX + "Oh no! You're overencumbered! You will move slower from now on");
		}
		
		if(isEncumbered) {
			if(currentWeight < maxWeight) {
				Main.getInstance().getDatabase("users").setPlayerData(player, new String[] {"is_encumbered"}, new Integer[] {0});
				player.removePotionEffect(PotionEffectType.SLOW);
				Util.resetDiseaseEffects(player);
				player.sendMessage(Util.CARRY_WEIGHT_PREFIX + "Yay! You're no longer overencumbered!");
			}
		}
	}
	
	/**
	 * Calculate carry weight multiplier for a given item based on its ingredients
	 * @param ingredients - the items ingredients (from crafting recipe)
	 * @return - the carry weight multiplier
	 */
	private double calculateCarryMultiplier(List<ItemStack> ingredients) {
		double multiplier = 1;
		
		for(ItemStack ingredient :  ingredients) {

			for(Map.Entry<Material, Double> materialMultiplier : materialMultipliers.entrySet()) {
				if(ingredient.getType() == materialMultiplier.getKey()) {
					multiplier += materialMultiplier.getValue();
				}
				
			}
			
		}
		
		return multiplier;
	}
	
	
}
