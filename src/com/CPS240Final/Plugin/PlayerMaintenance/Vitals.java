package com.CPS240Final.Plugin.PlayerMaintenance;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Util.Util;
/**
 * 
 * @authors Owen Smith (Thirst) / Gary Pinsky (Breathing)
 *
 *This class serves as the vitals functionality for our plugin.
 *When users join, thirst is set to 100 and must be maintained 
 *in order to survive. This can be accomplished by drinking 
 *Various beverages in the game will increase your thirst.
 *Breathing is maintained by pressing shift repeatedly to 
 *increase the player's thirst score, which is kept on the
 *vitals scoreboard.
 *
 * 
 *
 *
 */
public class Vitals implements Listener {

	private static HashMap<UUID, Scoreboard> scoreboards = new HashMap<UUID, Scoreboard>();
	private static HashMap<Player, Integer> thirstRecord = new HashMap<Player, Integer>();
	private static HashMap<Player, Integer> breathRecord = new HashMap<Player, Integer>();
	
	private static HashMap<UUID, BukkitTask> playerTasks = new HashMap<UUID, BukkitTask>();
	
	public static final String THIRST_OBJECTIVE =  ChatColor.BLUE.toString() + ChatColor.BOLD + "o◯Thirst◯o";
	public static final String BREATH_OBJECTIVE = ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Breath";
	public static final String VITAL_TITLE = ChatColor.RED.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC.toString() + "....." +  ChatColor.RESET + ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString()  + "VITALS" + ChatColor.RED.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC + ".....";

	private static Vitals instance;
	
	private Vitals() {}
	
	/**
	 * @author Owen Smith
	 * @param p - Player whose information will be 
	 * displayed on the scoreboard
	 * Method initializes a scoreboard for each player
	 */
	public void initializeScoreboard(Player player) {
		
		int playerThirst = (Integer) Main.getInstance().getDatabase("users").getPlayerData(player, "thirst");
		thirstRecord.put(player, playerThirst);
		
		int playerBreath = (Integer) Main.getInstance().getDatabase("users").getPlayerData(player, "breath");
		breathRecord.put(player, playerBreath);
		
		runnable(player);
		
		if(!scoreboards.containsKey(player.getUniqueId())) {
			Scoreboard newScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
			newScoreboard.registerNewObjective("vitals", "dummy", VITAL_TITLE);
			scoreboards.put(player.getUniqueId(), newScoreboard);
		}

		Scoreboard scoreboard = scoreboards.get(player.getUniqueId());
		Objective objective = scoreboard.getObjective("vitals");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		Set<String> entries;

		entries = scoreboard.getEntries();

		for(String entry : entries) {
			scoreboard.resetScores(entry);
		}

		player.setScoreboard(scoreboard);
		setScore(player);
	}
	
	/**
	 * @author Owen Smith
	 * @param event - represents the event that occurs when a player leaves
	 * Save player's scores to the backend database on leave
	 */
	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		Main.getInstance().getServer().getScheduler().cancelTask(playerTasks.get(player.getUniqueId()).getTaskId());
		playerTasks.remove(player.getUniqueId());
		Main.getInstance().getDatabase("users").setPlayerData(player, new String[] {"thirst"}, new Integer[] {thirstRecord.get(player)});
	}
	
	/**
	 * @author Owen Smith
	 * @param player - player to add thirst to
	 * @param amount - amount to be added
	 * @return updated player thirst
	 */
	private int addThirst(Player player, int amount) {
		int playerThirst = thirstRecord.get(player);
		if (playerThirst + amount >= 100) {
			playerThirst = 100;
		} else {
			playerThirst += amount;
		}
		return playerThirst;
	}
	
	/**
	 * @author Owen Smith
	 * @param player - player whose score will be added to the scoreboard
	 */
	public void setScore(Player player) {
		Scoreboard scoreboard = scoreboards.get(player.getUniqueId());
		Objective vitalObjective = scoreboard.getObjective("vitals");

		vitalObjective.setDisplayName(VITAL_TITLE);
		vitalObjective.setDisplaySlot(DisplaySlot.SIDEBAR);

		Set<String> entries = scoreboard.getEntries();
		for(String entry : entries) {
			scoreboard.resetScores(entry);
		}

		vitalObjective.getScore(String.format("%-20s%3d", THIRST_OBJECTIVE + ChatColor.AQUA, thirstRecord.get(player))).setScore(2);
		vitalObjective.getScore(String.format("%-20s%5d", BREATH_OBJECTIVE + ChatColor.GREEN, breathRecord.get(player))).setScore(1);
		
		player.setScoreboard(scoreboard);
	}
	/**
	 * @author Owen Smith
	 * @param event - player consume event
	 * Updates the thirst based on what the player consumes
	 */
	@EventHandler
	public void handlePlayerDrinkEvent(PlayerItemConsumeEvent event) {
		Player player = event.getPlayer();
		ItemStack item = event.getItem();
		Integer playerThirst = thirstRecord.get(player);
		ItemStack hydroFlask = Util.HYDRO_FLASK;
		Material milk = Material.MILK_BUCKET;
		Material stew = Material.MUSHROOM_STEW;
		
		
		
		
		if (playerThirst >= 100) {
			event.setCancelled(true);
			player.sendMessage("Your thirst meter is full!");
			return;
		}
		else if (item.getType() == milk) {
			playerThirst = addThirst(player, Main.getInstance().getConfig().getInt("Thirst.ThirstValues.Milk"));
			
			
		}
		else if (item.getType() == stew) {
			playerThirst = addThirst(player, Main.getInstance().getConfig().getInt("Thirst.ThirstValues.MushroomStew"));
		}
		else if (item.getItemMeta() instanceof PotionMeta) {
			PotionMeta potion = (PotionMeta) item.getItemMeta();
			if (potion.getBasePotionData().getType() == PotionType.WATER) {
				playerThirst = addThirst(player, Main.getInstance().getConfig().getInt("Thirst.ThirstValues.WaterBottle"));
			} 
			else if (potion.getBasePotionData().getType() == PotionType.INSTANT_HEAL) {
				playerThirst = addThirst(player, Main.getInstance().getConfig().getInt("Thirst.ThirstValues.HealthPotion"));
			}
			
		}
		
		
		if (event.getItem().getItemMeta().getDisplayName().equals(hydroFlask.getItemMeta().getDisplayName())) {
			int flaskMaxUses = (int) Main.getInstance().getDatabase("customitems").getPlayerData(player, "hydroflask_max");
			int flaskCurrentUses = (int) Main.getInstance().getDatabase("customitems").getPlayerData(player, "hydroflask_uses");
			event.setCancelled(true);
			if (flaskCurrentUses == flaskMaxUses) {
				player.sendMessage(ChatColor.YELLOW + "sksksksksk!" + ChatColor.LIGHT_PURPLE + " Hold on sis! Your Hydroflask is empty!");
				return;
			} else {
				playerThirst = addThirst(player, Main.getInstance().getConfig().getInt("Thirst.ThirstValues.WaterBottle"));
				flaskCurrentUses++;
				Main.getInstance().getDatabase("customitems").setPlayerData(player, new String[] {"hydroflask_uses"}, new Integer[] {flaskCurrentUses});
			}
		}
		thirstRecord.replace(player, playerThirst);
	}
	
	/**
	 * @author Gary Pinsky
	 * @param event - player crouch event
	 * Updates player's breath on crouch event
	 */
	@EventHandler
	public void handlePlayerBreathEvent(PlayerToggleSneakEvent event) {
		Player player = event.getPlayer();
		Integer playerBreath = breathRecord.get(player);
		
		playerBreath = 100;
		
		breathRecord.replace(player, playerBreath);
		setScore(player);
	}
	
	/**
	 * @author Owen Smith
	 * @param event - player respawn event
	 * Method resets player's stats on respawn
	 */
	@EventHandler 
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		Integer playerThirst = thirstRecord.get(player);
		Integer playerBreath = breathRecord.get(player);
		playerThirst = 100;
		playerBreath = 100;
		thirstRecord.replace(player, playerThirst);
		breathRecord.replace(player, playerBreath);
		setScore(player);	
	}
	
	/**
	 * @author Gary Pinsky
	 * @param player - player whose breath will be updated
	 */
	private void makeHaveToBreath(Player player) {
		Integer playerBreath = breathRecord.get(player);
		
		if (playerBreath <= 0) {
			playerBreath = 0;
			player.damage(1);
		}
		else {
			if (player.getInventory().getChestplate() != null && player.getInventory().getChestplate().getItemMeta().getDisplayName().contentEquals(Util.RESPIRATOR.getItemMeta().getDisplayName())) {
				playerBreath -= 2;
			} else {
				
				if (player.isSprinting()) {
					playerBreath -= 10;
				} 
				
				playerBreath -= 30;
				
			}
		}
		
		breathRecord.replace(player, playerBreath);
		setScore(player);
	}
	
	/**
	 * @author Owen Smith
	 * @param event - player interact event
	 * Handles player hydroflask refill
	 */
   @EventHandler
    public void onPlayerInteractWithWater(PlayerInteractEvent event) {
	    Block water = event.getClickedBlock();
	    Player player = event.getPlayer();
	    if (player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(Util.HYDRO_FLASK.getItemMeta().getDisplayName()) || player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(Util.HYDRO_FLASK)) {
	        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
	            List<Block> lineOfSight = player.getLineOfSight(null, 5);
	            for (Block b : lineOfSight) {
	                if (b.getType() == Material.WATER) {
	                    Main.getInstance().getDatabase("customitems").setPlayerData(player, new String[] {"hydroflask_uses"}, new Integer[] {0});
	                    player.sendMessage("HydroFlask has been refilled :D");                  
	                }
	            }
	        }
	    }
    }
	/**
	 * @author Owen Smith
	 * @param player - player whose thirst will be updated
	 * Updates player's thirst, for use in a runnable
	 */
	private void makeThirsty(Player player) {
		Integer playerThirst = thirstRecord.get(player);
		Biome playerBiome = player.getLocation().getBlock().getBiome();
		
		if (playerThirst <= 0) {
			playerThirst = 0;
			player.damage(10);
		}
		else {
			if (player.isSprinting()) {
				playerThirst -= 2;
			} 
			if (playerBiome == Biome.DESERT || playerBiome == Biome.DESERT_HILLS || playerBiome == Biome.DESERT_LAKES) {
				playerThirst -= 5;
			}  else if (playerBiome == Biome.SNOWY_TUNDRA || playerBiome == Biome.COLD_OCEAN) {
				playerThirst -= 3;
			}
			
			playerThirst -= 2;
		}
		
		thirstRecord.replace(player, playerThirst);
		setScore(player);
	}
	
	/**
	 * @author jeremyproksch
	 * @param player the player being acted on
	 * adds effects if a player does not sleep for one day
	 */
	public void changeSleep(Player player)
	{
		int time = player.getStatistic(Statistic.TIME_SINCE_REST);
		//assumes time is stored in ticks
		if(time > 24000)
		{
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 99999, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 99999, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 99999, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 99999, 1));
			player.sendMessage("You are exahusted and need to sleep.");
		}
	}
	
	/**
	 * @author Owen Smith
	 * @param player - player that operations will be performed on
	 * Updates player breath/thirst score every 60 server ticks (3 seconds)
	 */
	private void runnable(Player player) {
		
		BukkitTask runnable = new BukkitRunnable() {
		       
	        @Override
	        public void run() {
	        	setScore(player);
	        	makeHaveToBreath(player);
	        	makeThirsty(player);
	        	changeSleep(player);
	        }
	    }.runTaskTimer(Main.getInstance(), 0, 60);
	    playerTasks.put(player.getUniqueId(), runnable);
	}
	
	public static Vitals getInstance() {
		if(instance == null)
			instance = new Vitals();
		return instance;
	}
}
