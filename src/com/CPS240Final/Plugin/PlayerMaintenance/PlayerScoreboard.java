package com.CPS240Final.Plugin.PlayerMaintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class PlayerScoreboard implements Listener {

	private static HashMap<UUID, Scoreboard> scoreboards = new HashMap<UUID, Scoreboard>();
	private static List<String> objectiveNames = new ArrayList<String>();
	
	private static PlayerScoreboard instance;

	public static final String THIRST_OBJECTIVE =  ChatColor.BLUE + "o◯Thirst◯o ";
	public static final String BREATH_OBJECTIVE = ChatColor.WHITE + "Breath";
	public static final String VITAL_TITLE = ChatColor.RED.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC.toString() + "....." +  ChatColor.RESET + ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString()  + "VITALS" + ChatColor.RED.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC + ".....";

	private PlayerScoreboard() {}
	
	/**
	 * @author Owen Smith
	 * @param p - player to add scoreboard to
	 * Initializes a scoreboard for each player
	 */
	public void initializeScoreboard(Player p) {
		if(!scoreboards.containsKey(p.getUniqueId())) {
			Scoreboard newScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
			newScoreboard.registerNewObjective("vitals", "dummy", VITAL_TITLE);
			scoreboards.put(p.getUniqueId(), newScoreboard);
		}

		Scoreboard scoreboard = scoreboards.get(p.getUniqueId());
		Objective objective = scoreboard.getObjective("vitals");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		Set<String> entries;

		entries = scoreboard.getEntries();

		for(String entry : entries) {
			scoreboard.resetScores(entry);
		}

		p.setScoreboard(scoreboard);
	}
	
	/**
	 * @author Owen Smith
	 * @param p - player to add score to
	 * @param objective - objective to be added to the scoreboard
	 * @param score - score to be added to the scoreboard
	 * @param scoreboardOrder - position of objective on scoreboard
	 * Adds a score to player's scoreboard
	 */
	public void setScore(Player p, String objective, int score, int scoreboardOrder) {
		Scoreboard scoreboard = scoreboards.get(p.getUniqueId());
		Objective vitalObjective = scoreboard.getObjective("vitals");

		vitalObjective.setDisplayName(VITAL_TITLE);
		vitalObjective.setDisplaySlot(DisplaySlot.SIDEBAR);

		Set<String> entries = scoreboard.getEntries();
		for(String entry : entries) {
			scoreboard.resetScores(entry);
		}

		vitalObjective.getScore(objective + ChatColor.AQUA + String.format("%8d", score)).setScore(scoreboardOrder);
		p.setScoreboard(scoreboard);
	}

	/**
	 * @author Owen Smith
	 * @return - return instance of scoreboard
	 * Get instance of scoreboard
	 */
	public static PlayerScoreboard getInstance() {
		if(instance == null)
			instance = new PlayerScoreboard();
		return instance;
	}
	
	/**
	 * @author Owen Smith
	 * @param e - player quit event
	 * Remove scoreboard from hashmap
	 */
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		scoreboards.remove(p.getUniqueId());
	}


}
