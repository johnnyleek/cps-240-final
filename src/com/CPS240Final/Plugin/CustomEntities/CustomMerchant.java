package com.CPS240Final.Plugin.CustomEntities;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Merchant;
import org.bukkit.inventory.MerchantRecipe;

import com.CPS240Final.Plugin.Main;

/**
 * A custom merchant (villager) which has unique trades (custom items)
 * @author Johnny Leek
 *
 */
public class CustomMerchant extends CustomEntity {
	
	//The instance of the villager and it's profession (outfit)
	private Villager villager;
	private Profession profession;
	
	//The Merchant GUI to open on interact (holds trades)
	private Merchant villagerGUI;
	
	//The list of villager trades
	private List<MerchantRecipe> trades = new ArrayList<MerchantRecipe>();
	
	//The name of the villager
	private String name;
	
	//The "shop" name, the inventory name
	private String shopName;
	
	/**
	 * Constructor to create a new custom merchant
	 * @param name - the name to display above the villager's head
	 * @param profession - the outfit of the villager
	 * @param shopName - the name of the inventory that opens on interact
	 */
	public CustomMerchant(String name, Profession profession, String shopName) {
		super(CustomEntityType.MERCHANT);
		this.name = name;
		this.profession = profession;	
		this.shopName = shopName;
	}
	
	/**
	 * Instantiates a new villager GUI and opens it for the player that clicked
	 * @param p - The player that clicked
	 */
	public void onInteract(Player p) {
		villagerGUI = Bukkit.createMerchant(shopName);
		villagerGUI.setRecipes(trades);
		p.openMerchant(villagerGUI, true);
	}
	
	/**
	 * Adds a trade to the villager
	 * @param item - the item to add
	 * @param uses - how many times the trade can be used
	 * @param ingredients - an array (of at most 2) items that must be given in order to receive the trade (price)
	 */
	public void addTrade(ItemStack item, int uses, ItemStack...ingredients) {
		
		MerchantRecipe trade = new MerchantRecipe(item, uses);
		trade.setExperienceReward(false);
		
		trade.addIngredient(ingredients[0]);
		if(ingredients.length > 1) trade.addIngredient(ingredients[1]);
		
		trades.add(trade);
	}
	
	/**
	 * Spawns the villager at the given location
	 * @param loc - the location to spawn the villager
	 */
	public void spawnEntity(Location loc) {
		
		villager = (Villager)Bukkit.getWorld("world").spawnEntity(loc, EntityType.VILLAGER);
		villager.setCustomName(name);
		villager.setProfession(profession);
		villager.setVillagerExperience(1);
		villager.setInvulnerable(super.canTakeDamage());
		
		super.setEntity(villager);
		Main.getInstance().getCustomEntities().put(this, CustomEntityType.MERCHANT);
		
	}
	
}
