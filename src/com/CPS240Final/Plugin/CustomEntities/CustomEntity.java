package com.CPS240Final.Plugin.CustomEntities;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Custom entity class, this allows us to store entities with custom properties
 * (such as custom right click events, and names) on the server
 * @author Johnny Leek
 *
 */
public abstract class CustomEntity {
	
	//The entity itself
	private Entity entity;
	
	//The type of entity
	private CustomEntityType entityType;
	
	//Whether or not the entity can take damage
	private boolean canTakeDamage = true;
	
	/**
	 * Instantiate a new custom entity
	 * @param entityType - the type of entity
	 */
	public CustomEntity(CustomEntityType entityType) {
		this.entityType = entityType;
	}
	
	/**
	 * Spawns the entity at a specified location
	 * @param loc - the location to spawn the entity
	 */
	public abstract void spawnEntity(Location loc);
	
	/**
	 * What to do when the user interacts (right clicks) the entity
	 * @param p - the player that interacted
	 */
	public abstract void onInteract(Player p);
	
	/* Getters and setters */
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
	public Entity getEntity() {
		return entity;
	}

	public CustomEntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(CustomEntityType entityType) {
		this.entityType = entityType;
	}

	public boolean canTakeDamage() {
		return canTakeDamage;
	}

	public void setCanTakeDamage(boolean canTakeDamage) {
		this.canTakeDamage = canTakeDamage;
	}
	
}
