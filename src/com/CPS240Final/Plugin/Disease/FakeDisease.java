package com.CPS240Final.Plugin.Disease;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.potion.PotionEffect;

import com.CPS240Final.Plugin.Util.Util;

/**
 * This is a "fake disease" that is never shown to the
 * user, but to represent a disease with any name. It is
 * used in diseases with temporary effects (Ex. SpiderBite),
 * to prevent an infinite scheduling loop.
 * @author Johnny Leek
 *
 */
public class FakeDisease extends Disease{

	public FakeDisease(String name) {
		super(name, 
				  new ArrayList<PotionEffect>(), 
				  Util.createItem(Material.BARRIER, 
						  		  ChatColor.YELLOW + "FAKE", 
						  		  ChatColor.LIGHT_PURPLE + "Fake Disease"),
				  null,
				  null,
				  null
				 );
	}
	
}
