package com.CPS240Final.Plugin.Disease;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CPS240Final.Plugin.Util.Util;

/**
 * An example blindness disease
 * @author Johnny Leek
 */
public class ExampleDisease extends Disease {

	public ExampleDisease(Player player) {
		super(ChatColor.RED + "Blindness", 
			  new ArrayList<PotionEffect>(Arrays.asList(new PotionEffect(PotionEffectType.BLINDNESS, 1000000, 1))), 
			  Util.createItem(Material.ENDER_EYE, 
					  		  ChatColor.RED + "Blindness", 
					  		  ChatColor.LIGHT_PURPLE + "CAN'T SEE"),
			  "blind",
			  player,
			  "saw the dark until they saw the light"
			 );
	}

	
	
}
