package com.CPS240Final.Plugin.Disease;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CPS240Final.Plugin.Util.Util;

/**
 * 
 * @author Owen Smith
 * 
 * Class serves as the model for the coronavirus disease.
 * Disease comes from bat meat, which is dropped by bats.
 *
 */
public class CoronaVirus extends Disease {
	

		public CoronaVirus(Player player) {
			super(ChatColor.DARK_PURPLE + "COVID-19", 
				  new ArrayList<PotionEffect>(Arrays.asList(new PotionEffect(PotionEffectType.HARM, 1000000, 0),
						  									new PotionEffect(PotionEffectType.SLOW, 1000000, 0),
						  									new PotionEffect(PotionEffectType.WEAKNESS, 1000000, 0))), 
				  Util.createItem(Material.DRIED_KELP, 
						  		  ChatColor.DARK_PURPLE + "COVID-19", 
						  		  ChatColor.LIGHT_PURPLE + "Really? Coronavirus? Dead joke, devs..."),
				  "covid",
				  player,
				  "forgot to wash their hands"
				 );
		}
	}
