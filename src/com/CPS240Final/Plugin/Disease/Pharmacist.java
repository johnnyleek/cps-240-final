package com.CPS240Final.Plugin.Disease;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.CustomEntities.CustomMerchant;
import com.CPS240Final.Plugin.Util.Util;

import net.md_5.bungee.api.ChatColor;

/**
 * A pharmacist who sells medicine for diseases
 * @author Johnny Leek
 *
 */
public class Pharmacist extends CustomMerchant {
	
	//The possible medicine that the pharmacist can sell
	private List<ItemStack> possibleMedicines = new ArrayList<ItemStack>();
	
	/**
	 * Creates the pharmacist
	 */
	public Pharmacist() {
		super(ChatColor.AQUA.toString() + ChatColor.BOLD + "PHARMACIST",
			  Profession.LIBRARIAN,
			  ChatColor.YELLOW.toString() + ChatColor.BOLD + "BUY MEDICINE | PHARMACIST");
		
		//Adds a medicine for each disease as a trade to the pharmacist
		for(Disease disease : Main.getInstance().getPossibleDiseases()) {
			possibleMedicines.add(
					Util.createPotion(
							disease.getName() + " " + Util.MEDICINE_NAME,
							Color.fromRGB(Util.randomNum(0, 255), Util.randomNum(0, 255), Util.randomNum(0, 255)),
							PotionType.AWKWARD,
							ChatColor.GREEN + "A magical potion that will cure disease!",
							ChatColor.AQUA + "Cures:", disease.getName())
					);
		}
		
		//Adds a mystery potion to the pharmacist
		possibleMedicines.add(
				Util.createPotion(
						ChatColor.WHITE.toString() + ChatColor.BOLD + "???",
						Color.fromRGB(0, 0, 0),
						PotionType.AWKWARD,
						ChatColor.AQUA.toString() + ChatColor.MAGIC + "Who knows that this does?")
				);
		
		//Randomizes the amount of trades (and what trades they are
		for(int i = 0; i < Util.randomNum(1, 4); i++) {
			ItemStack medicine = possibleMedicines.get(Util.randomNum(0, possibleMedicines.size() - 1));
			super.addTrade(
					medicine,
					1,
					new ItemStack(Material.EMERALD, 5)
				);
			possibleMedicines.remove(medicine);
		}
		
	}

	
}
