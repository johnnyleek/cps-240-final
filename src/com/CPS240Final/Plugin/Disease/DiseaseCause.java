package com.CPS240Final.Plugin.Disease;

/**
 * The cause of the disease
 * @author Johnny Leek
 *
 */
public enum DiseaseCause {

	COMMAND,
	ENEMY,
	FOOD
	
}
