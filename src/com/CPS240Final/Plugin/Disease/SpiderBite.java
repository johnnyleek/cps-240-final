package com.CPS240Final.Plugin.Disease;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Util.Util;

/**
 * A spider bite disease
 * @author Johnny Leek
 *
 */
public class SpiderBite extends Disease {

	public SpiderBite(Player player) {
		super(ChatColor.YELLOW + "Spider Bite", 
				  new ArrayList<PotionEffect>(Arrays.asList(new PotionEffect(PotionEffectType.POISON, 400, 1),
						  									new PotionEffect(PotionEffectType.GLOWING, 400, 1))), 
				  Util.createItem(Material.FERMENTED_SPIDER_EYE, 
						  		  ChatColor.YELLOW + "Spider Bite", 
						  		  ChatColor.LIGHT_PURPLE + "The spiders venom is poisoning you!"),
				  "spider",
				  player,
				  "wanted to become Spiderman"
				 );
		Main.getInstance().getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			@Override
			public void run() {
				try {
					Util.removeDisease(player, new FakeDisease(ChatColor.YELLOW + "Spider Bite"));
				} catch(NullPointerException e) {
					//Do nothing. This is thrown because we instantiate the disease initially with a null user
				}
			}
		}, 400L);
	}
	
}
