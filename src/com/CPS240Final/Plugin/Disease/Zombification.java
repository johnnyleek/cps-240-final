package com.CPS240Final.Plugin.Disease;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CPS240Final.Plugin.Util.Util;

/**
 * A zombification disease (getting hit by zombie)
 * @author Johnny Leek
 *
 */
public class Zombification extends Disease {

	public Zombification(Player player) {
		super(ChatColor.DARK_AQUA + "Zombification", 
			  new ArrayList<PotionEffect>(Arrays.asList(new PotionEffect(PotionEffectType.HUNGER, 1000000, 1),
					  									new PotionEffect(PotionEffectType.SLOW, 1000000, 0),
					  									new PotionEffect(PotionEffectType.WEAKNESS, 1000000, 0))), 
			  Util.createItem(Material.ROTTEN_FLESH, 
					  		  ChatColor.LIGHT_PURPLE + "Zombification", 
					  		  ChatColor.LIGHT_PURPLE + "That zombie bite really hurt!"),
			  "zombie",
			  player,
			  "gained a sudden love for the taste of brains"
			 );
	}
	
}
