package com.CPS240Final.Plugin.Disease;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

/**
 * An abstract disease representing a player contractable disease
 * @author Johnny Leek
 *
 */
public abstract class Disease {
	
	//The name of the disease
	private String name;
	
	//The "command name" (The name to type in the /adddisease or /removedisease command
	private String cmdName;
	
	//A list of potion effects that the disease gives
	private List<PotionEffect> effects;
	
	//The icon that should appear in /listdisease
	private ItemStack icon;
	
	//The player that is affected by the disease
	private Player affectedPlayer;
	
	//The death message to display when the user dies from the disease
	private String deathMessage;
	
	/**
	 * Creates a new disease
	 * @param name - the name of the disease
	 * @param effects - the effects to give the player
	 * @param icon - the icon in /listdisease
	 * @param cmdName /the name to put in the command
	 * @param affectedPlayer - the player affected by the disease
	 * @param deathMessage - the death message when the user dies from the disease
	 */
	public Disease(String name, List<PotionEffect> effects, ItemStack icon, String cmdName, Player affectedPlayer, String deathMessage) {
		this.name = name;
		this.effects = effects;
		this.icon = icon;
		this.cmdName = cmdName;
		this.affectedPlayer = affectedPlayer;
		this.deathMessage = deathMessage;
	}

	
	/* Getters and Setters */
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PotionEffect> getEffects() {
		return effects;
	}

	public void setEffects(List<PotionEffect> effects) {
		this.effects = effects;
	}

	public ItemStack getIcon() {
		return icon;
	}

	public void setIcon(ItemStack icon) {
		this.icon = icon;
	}
	
	public String getCmdName() {
		return cmdName;
	}

	public void setCmdName(String cmdName) {
		this.cmdName = cmdName;
	}

	public String getDeathMessage() {
		return deathMessage;
	}

	public void setDeathMessage(String deathMessage) {
		this.deathMessage = deathMessage;
	}	
}
