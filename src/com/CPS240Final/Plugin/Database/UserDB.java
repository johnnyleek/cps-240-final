package com.CPS240Final.Plugin.Database;

import java.sql.SQLException;
import java.sql.Statement;

import com.CPS240Final.Plugin.Main;

/**
 * Users for the database
 * @author Johnny Leek
 *
 */
public class UserDB extends Database {

	//SQL Query to create Custom Table
	private String createTableQuery;
	
	/**
	 * Creates a new Database with the table name from the config
	 */
	public UserDB() {
		super(Main.getInstance().getConfig().getString("Database.Tables.User"));
		
		createTableQuery = String.format(
				"CREATE TABLE IF NOT EXISTS %s (" +
				"`player` varchar(64) NOT NULL," +
				"`diseases` varchar(32767) DEFAULT ''," +
				"`thirst` int(11) DEFAULT 100," +
				"`breath` int(11) DEFAULT 100," +
				"`current_carryweight` int(11) DEFAULT 0," +
				"`max_carryweight` int(11) DEFAULT 300," +
				"`is_encumbered` int(11) DEFAULT 0," +
				"`in_inventory` int(11) DEFAULT 0," +
				"PRIMARY KEY (`player`)"
				+ ");"
				, instance.getConfig().getString("Database.Tables.User"));
	}
	
	/**
	 * Initializes the connection to the table in the database
	 */
	@Override
	public void initialize() {
		dbConnection = getConnection();
		try {
			Statement createTable = dbConnection.createStatement();
			createTable.executeUpdate(createTableQuery);
			createTable.close();
		} catch(SQLException e) {
			instance.getLogger().severe("Could not create new table.\n");
			e.printStackTrace();
		}
		dbConnection = getConnection();
	}
	
}
