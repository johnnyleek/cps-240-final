package com.CPS240Final.Plugin.Database;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;

import com.CPS240Final.Plugin.Main;

/**
 * SQLite Database class to allow storage of data
 * @author Johnny Leek
 *
 */
public abstract class Database {
	
	//Instance of main plugin (to access config and logging functionality)
	public Main instance;
	Connection dbConnection;
	
	//Table to access data from
	private String table;
	
	public Database(String table) {
		this.instance = Main.getInstance();
		this.table = table;
	}
	
	/**
	 * Creates database if it doesn't exist, then creates a connection to it
	 * @return SQL Connection to database
	 */
	public Connection getConnection() {
		//Create db file if it doesn't already exist
		File db = new File(Main.getInstance().getDataFolder(), "Database.db");
		if(!db.exists()) {
			try {
				db.createNewFile();
			} catch(IOException e) {
				instance.getLogger().severe("Could not create database.");
			}
		}
		
		//Create new connection if one doesn't already exist and return it
		try {
			if(dbConnection != null && !dbConnection.isClosed()) {
				return dbConnection;
			}
			Class.forName("org.sqlite.JDBC");
			dbConnection = DriverManager.getConnection("jdbc:sqlite:" + db);
			return dbConnection;
		} catch(SQLException e) {
			instance.getLogger().severe("SQLite exception...");
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			instance.getLogger().severe("Could not locate SQLite JDBC Library.");
		}
		return null;
	}
	
	/**
	 * Creates the current table if one doesn't already exist
	 * and initializes connection
	 */
	public abstract void initialize();
	
	/**
	 * Gets the data at a column for a provided player
	 * @param player - The player from which to retrieve the data
	 * @param dataColumn - The column to get the data from
	 * @return - An object representing the data retrieved
	 */
	@SuppressWarnings("unchecked")
	public <T> T getPlayerData(Player player, String dataColumn) {
		
		Connection connection = null;
		PreparedStatement query = null;
		ResultSet results = null;
		
		try {
			//Gets the DB Connection
			connection = getConnection();
			
			//Executes SQL Query to grab data from specified location
			query = connection.prepareStatement(String.format("SELECT * FROM %s WHERE player = '%s';", table, player.getUniqueId().toString()));
			results = query.executeQuery();
			
			//Read through all DB Results
			while(results.next()) {
				
				//If the result is for the given player, return that data
				if(results.getString("player").equalsIgnoreCase(player.getUniqueId().toString())) {
					return (T) results.getObject(dataColumn);
				}
			}
			
		} catch(SQLException e) {
			//Print Execute Query error message
			instance.getLogger().severe(instance.getConfig().getString("Database.Errors.ExecuteQuery"));
			instance.getLogger().severe(e.toString());
		} finally {
			//Attempt to close connections
			try {
				if(query != null) query.close();
				if(connection != null) connection.close();
			} catch(SQLException e) {
				//Print Close Connection error message
				instance.getLogger().severe(instance.getConfig().getString("Database.Errors.CloseConnection"));
				instance.getLogger().severe(e.toString());
			}
		}
		return null;
	}
	
	/**
	 * Sets the data columns in the database for a given player
	 * @param player - The player for which to set the data
	 * @param dataColumns - Array of column names (as string)
	 * @param data - Array of data to input into columns (same order as dataColumns)
	 */
	public <T> void setPlayerData(Player player, String[] dataColumns, T[] data) {
		Connection connection = null;
		PreparedStatement query = null;
		
		//If the lengths of dataColumns and data is not the same, throw an exception and do not continue
		if(dataColumns.length != data.length) {
			instance.getLogger().severe(instance.getConfig().getString("Database.Errors.ParameterMismatch"));
			return;
		}
		
		try {
			connection = getConnection();
			
			//Build the query and value strings for the SQL Query
			//Example Query: (test_col_1, test_col_3)
			//Example Values:(100, 500)
			String queryColumn = "(player,";
			String valueColumn = "('" + player.getUniqueId().toString() + "',";
			String updateQueryStr = "";
			for(int i = 0; i < dataColumns.length - 1; i++) {
				queryColumn += dataColumns[i] + ",";
				valueColumn += data[i] + ",";
				updateQueryStr = dataColumns[i] + "=" + data[i] + ",";
			}
			
			queryColumn += dataColumns[dataColumns.length - 1] + ")";
			valueColumn += data[data.length - 1] + ")";
			updateQueryStr = dataColumns[dataColumns.length - 1] + "=" + data[data.length - 1];
			
			/*instance.getLogger().info(String.format(
					"INSERT OR IGNORE INTO %s%s VALUES%s;" +
					"UPDATE %s SET %s WHERE player='%s';",
					table, queryColumn, valueColumn,
					table, updateQueryStr, player.getUniqueId().toString()));*/
			
			//Prepare query as SQL
			query = connection.prepareStatement(String.format(
					"INSERT OR IGNORE INTO %s%s VALUES%s;",
					table, queryColumn, valueColumn));
			
			//Execute Query
			query.executeUpdate();
			
			query.close();
			query = connection.prepareStatement(String.format(
					"UPDATE %s SET %s WHERE player='%s';",
					table, updateQueryStr, player.getUniqueId().toString()));
			
			query.executeUpdate();
			return;
		} catch(SQLException e) {
			//Print Execute Query error message
			instance.getLogger().severe(instance.getConfig().getString("Database.Errors.ExecuteQuery"));
			instance.getLogger().severe(e.toString());
		} finally {
			//Attempt to close connections
			try {
				if(query != null) query.close();
				if(connection != null) connection.close();
			} catch(SQLException e) {
				//Print Close Connection error message
				instance.getLogger().severe(instance.getConfig().getString("Database.Errors.CloseConnection"));
				instance.getLogger().severe(e.toString());
			}
		}
	}
}
