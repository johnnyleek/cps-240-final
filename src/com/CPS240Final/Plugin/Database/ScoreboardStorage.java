package com.CPS240Final.Plugin.Database;

/**
 * Initializes a new serialized storage to store scoreboards
 * @author Johnny Leek
 *
 */
public class ScoreboardStorage extends SerializedStorage {

	public ScoreboardStorage() {
		super("scoreboards", "scoreboard");
	}
	
}
