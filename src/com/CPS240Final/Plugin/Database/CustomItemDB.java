package com.CPS240Final.Plugin.Database;

import java.sql.SQLException;
import java.sql.Statement;

import com.CPS240Final.Plugin.Main;
/**
 * Custom Items Table for the database
 * @author Owen Smith
 *
 */
public class CustomItemDB extends Database {
	//SQL Query to create Custom Table
	private String createTableQuery;
	
	/**
	 * Creates a new Database with the table name from the config
	 */
	public CustomItemDB() {
		super(Main.getInstance().getConfig().getString("Database.Tables.CustomItems"));
		
		createTableQuery = String.format(
                "CREATE TABLE IF NOT EXISTS %s (" +
          "`player` varchar(64) NOT NULL," +
          "`hydroflask_uses` int(11) DEFAULT 0," +
          "`hydroflask_max` int(11) DEFAULT 10," +
          "PRIMARY KEY (`player`)"
          + ");"
          , instance.getConfig().getString("Database.Tables.CustomItems"));
	}
	
	/**
	 * Initializes the connection to the table in the database
	 */
	@Override
	public void initialize() {
		dbConnection = getConnection();
		try {
			Statement createTable = dbConnection.createStatement();
			createTable.executeUpdate(createTableQuery);
			createTable.close();
		} catch(SQLException e) {
			instance.getLogger().severe("Could not create new table.\n");
			e.printStackTrace();
		}
		dbConnection = getConnection();
	}
	
}