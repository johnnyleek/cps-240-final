package com.CPS240Final.Plugin.Database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.bukkit.entity.Player;

import com.CPS240Final.Plugin.Main;

/**
 * Due to limitations of the SQLite database, we're also allowing
 * storage in the form of serialized objects on the servers local storage.
 * 
 * Each file is stored in a directory based on the object, the file name
 * being the players Unique ID (UUID). This ensures the same file doesn't
 * get created for 2 different players
 * @author Johnny Leek
 *
 */
public abstract class SerializedStorage {

	private final File DIRECTORY;
	private final String DIRECTORY_NAME;
	private final String EXTENSION;
	private final Main plugin;
	
	/**
	 * Constructor, allows to create a new storage reference based on the given
	 * directory name and extension.
	 * 
	 * Directory will be placed at path SERVER_DIR/PLUGIN_NAME/player_data/directoryName/
	 * @param directoryName - the directory name to store files in
	 * @param extension - the file extension of the data files
	 */
	public SerializedStorage(String directoryName, String extension) {
		
		this.DIRECTORY_NAME = directoryName;
		this.EXTENSION = extension;
		this.plugin = Main.getInstance();
		
		this.DIRECTORY = new File(plugin.getDataFolder() + File.separator + "player_data" + File.separator + this.DIRECTORY_NAME);
		if(!this.DIRECTORY.exists()) {
			this.DIRECTORY.mkdirs();
		}
		
	}
	
	/**
	 * Saves the data to the players file
	 * @param <T> - the data type (must be serializable)
	 * @param player - the player to store the object for
	 * @param data - the object to store
	 */
	public <T extends Serializable> void save(Player player, T data) {
		
		try {
			File playerFile = new File(DIRECTORY, String.format("%s.%s", player.getUniqueId().toString(), EXTENSION));
			FileOutputStream fileOutputStream = new FileOutputStream(playerFile);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			
			objectOutputStream.writeObject(data);
			objectOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Retrieves the object from the players file
	 * @param <T> - the data type (must be serializable)
	 * @param player - the player to retrieve the object from
	 * @return - the object in the file
	 */
	public <T extends Serializable> T read(Player player) {
		try {
			File playerFile = new File(DIRECTORY, String.format("%s.%s", player.getUniqueId().toString(), EXTENSION));
			FileInputStream fileInputStream = new FileInputStream(playerFile);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			T readData = (T) objectInputStream.readObject();
			
			objectInputStream.close();
			
			return readData;
		} catch(IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
