package com.CPS240Final.Plugin;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.RecipeChoice.MaterialChoice;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import com.CPS240Final.Plugin.Commands.AddDisease;
import com.CPS240Final.Plugin.Commands.ListUserDisease;
import com.CPS240Final.Plugin.Commands.RemoveDisease;
import com.CPS240Final.Plugin.CustomEntities.CustomEntity;
import com.CPS240Final.Plugin.CustomEntities.CustomEntityType;
import com.CPS240Final.Plugin.Database.CustomItemDB;
import com.CPS240Final.Plugin.Database.Database;
import com.CPS240Final.Plugin.Database.ScoreboardStorage;
import com.CPS240Final.Plugin.Database.SerializedStorage;
import com.CPS240Final.Plugin.Database.UserDB;
import com.CPS240Final.Plugin.Disease.CoronaVirus;
import com.CPS240Final.Plugin.Disease.Disease;
import com.CPS240Final.Plugin.Disease.ExampleDisease;
import com.CPS240Final.Plugin.Disease.SpiderBite;
import com.CPS240Final.Plugin.Disease.Zombification;
import com.CPS240Final.Plugin.Encumbrance.CarryWeight;
import com.CPS240Final.Plugin.Events.ChangeBlockDrops;
import com.CPS240Final.Plugin.Events.ChangeEnemyDamage;
import com.CPS240Final.Plugin.Events.ChangeItemDurability;
import com.CPS240Final.Plugin.Events.CustomEntityInteract;
import com.CPS240Final.Plugin.Events.DiseaseListener;
import com.CPS240Final.Plugin.Events.PlaceBlock;
import com.CPS240Final.Plugin.Events.PlayerJoin;
import com.CPS240Final.Plugin.GUIs.DiseaseGUI;
import com.CPS240Final.Plugin.PlayerMaintenance.Vitals;
import com.CPS240Final.Plugin.Util.Util;

import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin {

	private Database userDB, customItemDB;
	private SerializedStorage scoreboardStorage;

	private static Main instance;

	private List<Disease> possibleDiseases;
	private HashMap<CustomEntity, CustomEntityType> customEntities = new HashMap<CustomEntity, CustomEntityType>();

	private final String pluginPrefix = ChatColor.AQUA.toString() + ChatColor.BOLD + "[CPS 240 PLUGIN] ";
	public void onEnable() {
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		console.sendMessage(pluginPrefix + ChatColor.YELLOW + "Beginning plugin initialization...");
		instance = this;
		//Register Commands
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Initializing Commands...");
		this.getCommand("addDisease").setExecutor(new AddDisease());
		this.getCommand("listDisease").setExecutor(new ListUserDisease());
		this.getCommand("removeDisease").setExecutor(new RemoveDisease());
		console.sendMessage(ChatColor.GREEN + "Commands Initialized!");
		
		//Register Events
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Initializing Event Handlers...");
		this.getServer().getPluginManager().registerEvents(new ChangeItemDurability(), this);
		this.getServer().getPluginManager().registerEvents(new ChangeBlockDrops(), this);
		this.getServer().getPluginManager().registerEvents(new DiseaseGUI("Player", 9, "NONE"), this);
		this.getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
		this.getServer().getPluginManager().registerEvents(new DiseaseListener(), this);
		this.getServer().getPluginManager().registerEvents(Vitals.getInstance(), this);
		this.getServer().getPluginManager().registerEvents(new CustomEntityInteract(), this);
		this.getServer().getPluginManager().registerEvents(new PlaceBlock(), this);
		this.getServer().getPluginManager().registerEvents(new ChangeEnemyDamage(), this);
		this.getServer().getPluginManager().registerEvents(new CarryWeight(), this);
		console.sendMessage(ChatColor.GREEN + "Events Initialized");

		//Add Crafting Recipes
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Initializing Crafting Recipes...");
		ItemStack waterBottle = new ItemStack(Material.POTION);
		PotionMeta bottleMeta = (PotionMeta) waterBottle.getItemMeta();
		bottleMeta.setBasePotionData(new PotionData(PotionType.WATER));
		waterBottle.setItemMeta(bottleMeta);

		NamespacedKey waterBottleKey = new NamespacedKey(this, "waterBottleKey");
		ShapedRecipe waterBottleRecipe = new ShapedRecipe(waterBottleKey, waterBottle);
		waterBottleRecipe.shape("sss", "sbs", "sss");
		waterBottleRecipe.setIngredient('s', Material.SNOWBALL);
		waterBottleRecipe.setIngredient('b', Material.GLASS_BOTTLE);
		Bukkit.addRecipe(waterBottleRecipe);
		console.sendMessage(ChatColor.BLUE + "Water Bottle Recipe Added");
		
		
		ItemStack hydroFlask = Util.createPotion(ChatColor.YELLOW.toString() + ChatColor.MAGIC.toString() + ChatColor.BOLD + "...." + ChatColor.RESET + ChatColor.RED.toString() + ChatColor.BOLD + "  H" + ChatColor.YELLOW.toString() + ChatColor.BOLD + "Y" + ChatColor.GREEN.toString() + ChatColor.BOLD + "D" + ChatColor.AQUA.toString() + ChatColor.BOLD + "R" + ChatColor.BLUE.toString() + ChatColor.BOLD + "O" + ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "F" + ChatColor.RED.toString() + ChatColor.BOLD + "L" + ChatColor.YELLOW.toString() + ChatColor.BOLD + "A" + ChatColor.GREEN.toString() + ChatColor.BOLD + "S" + ChatColor.AQUA.toString() + ChatColor.BOLD + "K  " + ChatColor.YELLOW.toString() + ChatColor.MAGIC.toString() + ChatColor.BOLD + "....", Color.fromRGB(253, 253, 150), PotionType.AWKWARD, ChatColor.RED + "H" + ChatColor.YELLOW + "y" + ChatColor.GREEN + "d" + ChatColor.AQUA + "r" + ChatColor.BLUE + "o" + ChatColor.LIGHT_PURPLE + " " + ChatColor.RED + "F" + ChatColor.YELLOW + "l" + ChatColor.GREEN + "a" + ChatColor.AQUA + "s" + ChatColor.BLUE + "k" + ChatColor.LIGHT_PURPLE + "s" + ChatColor.RED + "," + ChatColor.YELLOW + " " + ChatColor.GREEN + "s" + ChatColor.AQUA + "e" + ChatColor.BLUE + "r" + ChatColor.LIGHT_PURPLE + "v" + ChatColor.RED + "i" + ChatColor.YELLOW + "n" + ChatColor.GREEN + "g" + ChatColor.AQUA + " " + ChatColor.BLUE + "t" + ChatColor.LIGHT_PURPLE + "h" + ChatColor.RED + "e"  , ChatColor.YELLOW + "m" + ChatColor.GREEN + "o" + ChatColor.AQUA + "s" + ChatColor.BLUE + "t" + ChatColor.LIGHT_PURPLE + " " + ChatColor.RED + "b" + ChatColor.YELLOW + "a" + ChatColor.GREEN + "s" + ChatColor.AQUA + "i" + ChatColor.BLUE + "c" + ChatColor.LIGHT_PURPLE + " " + ChatColor.RED + "o" + ChatColor.YELLOW + "f" + ChatColor.GREEN + " " + ChatColor.AQUA + "w" + ChatColor.BLUE + "h" + ChatColor.LIGHT_PURPLE + "i" + ChatColor.RED + "t" + ChatColor.YELLOW + "e" + ChatColor.GREEN + " " + ChatColor.AQUA + "g" + ChatColor.BLUE + "i" + ChatColor.LIGHT_PURPLE + "r" + ChatColor.RED + "l" + ChatColor.YELLOW + "s" + ChatColor.GREEN + " " + ChatColor.AQUA + "a" + ChatColor.BLUE + " " + ChatColor.LIGHT_PURPLE + "s" + ChatColor.RED + "o" + ChatColor.YELLOW + "l" + ChatColor.GREEN + "u" + ChatColor.AQUA + "t" + ChatColor.BLUE + "i" + ChatColor.LIGHT_PURPLE + "o" + ChatColor.RED + "n" + ChatColor.YELLOW + " "  , ChatColor.GREEN + "t" + ChatColor.AQUA + "o" + ChatColor.BLUE + " " + ChatColor.LIGHT_PURPLE + "t" + ChatColor.RED + "h" + ChatColor.YELLOW + "e" + ChatColor.GREEN + "i" + ChatColor.AQUA + "r" + ChatColor.BLUE + " " + ChatColor.LIGHT_PURPLE + "m" + ChatColor.RED + "o" + ChatColor.YELLOW + "s" + ChatColor.GREEN + "t" + ChatColor.AQUA + " " + ChatColor.BLUE + "b" + ChatColor.LIGHT_PURPLE + "a" + ChatColor.RED + "s" + ChatColor.YELLOW + "i" + ChatColor.GREEN + "c" + ChatColor.AQUA + " " + ChatColor.BLUE + "n" + ChatColor.LIGHT_PURPLE + "e" + ChatColor.RED + "e" + ChatColor.YELLOW + "d" + ChatColor.GREEN + "!" , ChatColor.AQUA + "#" + ChatColor.BLUE + "H" + ChatColor.LIGHT_PURPLE + "y" + ChatColor.RED + "d" + ChatColor.YELLOW + "r" + ChatColor.GREEN + "a" + ChatColor.AQUA + "t" + ChatColor.BLUE + "e" + ChatColor.LIGHT_PURPLE + "O" + ChatColor.RED + "r" + ChatColor.YELLOW + "D" + ChatColor.GREEN + "i" + ChatColor.AQUA + "e" + ChatColor.BLUE + "d" + ChatColor.LIGHT_PURPLE + "r" + ChatColor.RED + "a" + ChatColor.YELLOW + "t" + ChatColor.GREEN + "e");
		NamespacedKey hydroFlaskKey = new NamespacedKey(this, "hydroFlaskKey");
		ShapedRecipe hydroFlaskRecipe = new ShapedRecipe(hydroFlaskKey, hydroFlask);
		
		hydroFlaskRecipe.shape("aaa", "gag", "igi");
		hydroFlaskRecipe.setIngredient('a', Material.AIR);
		hydroFlaskRecipe.setIngredient('g', Material.GLASS);
		hydroFlaskRecipe.setIngredient('i', Material.IRON_BLOCK);
		Bukkit.addRecipe(hydroFlaskRecipe);
		console.sendMessage(ChatColor.BLUE + "HydroFlask Recipe Added");
		

		ItemStack pharmacistSummon = Util.PRESCRIPTION;

		NamespacedKey pharmacistSummonKey = new NamespacedKey(this, "pharmacistSummonKey");
		ShapedRecipe pharmacistSummonRecipe = new ShapedRecipe(pharmacistSummonKey, pharmacistSummon);
		pharmacistSummonRecipe.shape("oio", "ipi", "oio");
		pharmacistSummonRecipe.setIngredient('o', Material.AIR);
		pharmacistSummonRecipe.setIngredient('i', Material.INK_SAC);
		pharmacistSummonRecipe.setIngredient('p', Material.PAPER);
		Bukkit.addRecipe(pharmacistSummonRecipe);
		console.sendMessage(ChatColor.BLUE + "Pharmacist Summon Recipe Added");
		
		
		NamespacedKey respiratorKey = new NamespacedKey(this, "respiratorKey");
		ShapedRecipe respiratorRecipe = new ShapedRecipe(respiratorKey, Util.RESPIRATOR);
		respiratorRecipe.shape("gdg", "gig", "ggg");
		respiratorRecipe.setIngredient('i', Material.IRON_CHESTPLATE);
		respiratorRecipe.setIngredient('g', Material.GOLD_INGOT);
		respiratorRecipe.setIngredient('d', Material.DIAMOND);
		Bukkit.addRecipe(respiratorRecipe);
		console.sendMessage(ChatColor.BLUE + "Respirator Recipe Added");
		console.sendMessage(ChatColor.GREEN + "Crafting Recipes Initialized");
		
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Finding/Creating configuration file...");
		if(!this.getDataFolder().exists()) {
			this.getDataFolder().mkdirs();
		}

		File configFile = new File(this.getDataFolder(), "config.yml");
		if(!configFile.exists()) {
			try {
				configFile.createNewFile();
			} catch (IOException e) {
				Bukkit.getLogger().info(e.getMessage());
			}
		}
		console.sendMessage(ChatColor.GREEN + "Config File Found");
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Setting configuration file defaults (if they don't exist)...");
		this.getConfig().addDefault("Database.Tables.Test", "Test");
		this.getConfig().addDefault("Database.Tables.User", "User");
		this.getConfig().addDefault("Database.Tables.CustomItems", "CustomItems");
		this.getConfig().addDefault("Database.Errors.ExecuteQuery", "Failed to execute query.");
		this.getConfig().addDefault("Database.Errors.CloseConnection", "Failed to close SQL Connection.");
		this.getConfig().addDefault("Database.Errors.ParameterMismatch", "Length of dataColumns and data does not match.");
		this.getConfig().addDefault("Disease.DiseaseChange.Zombie", 5);
		this.getConfig().addDefault("Disease.DiseaseChange.Spider", 5);
		this.getConfig().addDefault("Thirst.ThirstValues.Milk", 25);
		this.getConfig().addDefault("Thirst.ThirstValues.WaterBottle", 35);
		this.getConfig().addDefault("Thirst.ThirstValues.HealthPotion", 55);
		this.getConfig().addDefault("Mobs.MobDamageModifier", 2);

		this.getConfig().addDefault("Thirst.ThirstValues.MushroomStew", 10);

		this.getConfig().addDefault("Ores.DropRates.Coal", 0.75);
		this.getConfig().addDefault("Ores.DropRates.Diamond", 0.10);
		this.getConfig().addDefault("Ores.DropRates.Emerald", 0.10);
		this.getConfig().addDefault("Ores.DropRates.Gold", 0.25);
		this.getConfig().addDefault("Ores.DropRates.Iron", 0.50);
		this.getConfig().addDefault("Ores.DropRates.Lapis", 0.30);
		this.getConfig().addDefault("Ores.DropRates.Redstone",0.30);

		this.getConfig().addDefault("CarryWeight.Multipliers.Coal", 0.3);
		this.getConfig().addDefault("CarryWeight.Multipliers.Iron", 0.5);
		this.getConfig().addDefault("CarryWeight.Multipliers.Gold", 0.3);
		this.getConfig().addDefault("CarryWeight.Multipliers.Diamond", 0.6);
		this.getConfig().addDefault("CarryWeight.Multipliers.Emerald", 0.6);
		this.getConfig().addDefault("CarryWeight.Multipliers.Stone", 0.5);
		this.getConfig().addDefault("CarryWeight.Multipliers.Wood", 0.4);
		
		this.getConfig().addDefault("CarryWeight.DefaultWeight", 1);
		
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		console.sendMessage(ChatColor.GREEN + "Saved config defaults as needed");
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Initializing database connections...");
		//Register DBs
		this.userDB = new UserDB();
		this.customItemDB = new CustomItemDB();

		this.userDB.initialize();
		this.customItemDB.initialize();
		console.sendMessage(ChatColor.GREEN + "Database connections initialized");
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Creating directories for serialized storage...");
		//Create Directories
		scoreboardStorage = new ScoreboardStorage();
		console.sendMessage(ChatColor.GREEN + "Directories added");
		console.sendMessage(ChatColor.LIGHT_PURPLE + "Adding diseases to disease lookup...");
		//Add diseases
		possibleDiseases = new ArrayList<Disease>(
							Arrays.asList(new ExampleDisease(null), new Zombification(null),
										  new SpiderBite(null), new CoronaVirus(null)));
		console.sendMessage(ChatColor.GREEN + "Diseases added");
		console.sendMessage(pluginPrefix + ChatColor.YELLOW + "CPS 240 Plugin Initialized! Have fun!");
	}

	public Database getDatabase(String name) {
		switch(name.toLowerCase()) {
		case "users":
			return userDB;
		case "customitems":
			return customItemDB;
		default:
			return null;
		}
	}

	public SerializedStorage getStorage(String name) {
		switch(name.toLowerCase()) {
		case "scoreboard":
			return scoreboardStorage;
		default:
			return null;
		}
	}

	public void onDisable() {
	}

	public static Main getInstance() {
		return instance;
	}

	public List<Disease> getPossibleDiseases() {
		return possibleDiseases;
	}

	public HashMap<CustomEntity, CustomEntityType> getCustomEntities() {
		return customEntities;
	}


}
