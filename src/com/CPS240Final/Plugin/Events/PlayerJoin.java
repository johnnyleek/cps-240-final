package com.CPS240Final.Plugin.Events;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Encumbrance.CarryWeight;
import com.CPS240Final.Plugin.PlayerMaintenance.Vitals;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

/**
 * Called when a player joins
 * @author Johnny Leek
 *
 */
public class PlayerJoin implements Listener{

	@EventHandler(priority=EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent e) {

		Main.getInstance().getLogger().info("Player joined!");
		e.getPlayer().teleport(new Location(Bukkit.getWorld("world"), 220.511, 71, 246.585));

		if(Main.getInstance().getDatabase("users").getPlayerData(e.getPlayer(), "player") == null) {
			Main.getInstance().getDatabase("users").setPlayerData(e.getPlayer(), new String[]{"diseases"}, new String[] {null});
			Main.getInstance().getDatabase("customitems").setPlayerData(e.getPlayer(), new String[]{"hydroflask_uses"}, new Integer[] {0});
			e.getPlayer().getInventory().addItem(new ItemStack(Material.STONE_SHOVEL));
			e.getPlayer().getInventory().addItem(new ItemStack(Material.STONE_PICKAXE));
			e.getPlayer().getInventory().addItem(new ItemStack(Material.STONE_AXE));
		}

		Vitals.getInstance().initializeScoreboard(e.getPlayer());
		CarryWeight.handleEncumbered(e.getPlayer());
	}

	private ItemStack getBook() {
		ItemStack introBook = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta bookMeta = (BookMeta) introBook.getItemMeta();

		List<BaseComponent[]> pages = new ArrayList<BaseComponent[]>();

		pages.add(
				new ComponentBuilder("").color(ChatColor.DARK_AQUA).bold(true).append("================").append("\n")
				.append("\n")
				.color(ChatColor.DARK_RED).bold(true).append("        ULTRA").append("\n")
				.color(ChatColor.DARK_RED).bold(true).append("      HARDCORE").append("\n")
				.append("\n")
				.color(ChatColor.DARK_PURPLE).bold(true).append("       CPS 240").append("\n")
				.append("\n")
				.color(ChatColor.GOLD).bold(true).append("    Johnny Leek").append("\n")
				.color(ChatColor.DARK_AQUA).bold(true).append("     Owen Smith").append("\n")
				.color(ChatColor.DARK_GREEN).bold(true).append("  Jeremy Proksch").append("\n")
				.color(ChatColor.DARK_BLUE).bold(true).append("     Gary Pinsky").append("\n\n\n")
				.bold(false).append("").color(ChatColor.DARK_AQUA).append("===================")
				.create()
				);


		bookMeta.spigot().setPages(pages);

		bookMeta.setTitle(ChatColor.LIGHT_PURPLE + "Intro Book");
		bookMeta.setAuthor("Me");
		introBook.setItemMeta(bookMeta);
		return introBook;
	}

}
