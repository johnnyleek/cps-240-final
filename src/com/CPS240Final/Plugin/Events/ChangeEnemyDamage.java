package com.CPS240Final.Plugin.Events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.CPS240Final.Plugin.Main;

public class ChangeEnemyDamage implements Listener {
	
	//multiply damage given by mobs by default 1.5, editable in config
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDamageByEntity(EntityDamageByEntityEvent event) {
		Entity entity = event.getEntity();
		Entity damager = event.getDamager();
		
		if (entity instanceof Player) {
			Player player  = (Player) entity;
			event.setDamage(event.getDamage() * Main.getInstance().getConfig().getDouble("Mobs.MobDamageModifier"));			
		}
	}
	
}
