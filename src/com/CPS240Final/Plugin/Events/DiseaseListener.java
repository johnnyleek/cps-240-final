package com.CPS240Final.Plugin.Events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Bat;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Disease.CoronaVirus;
import com.CPS240Final.Plugin.Disease.Disease;
import com.CPS240Final.Plugin.Disease.DiseaseCause;
import com.CPS240Final.Plugin.Disease.SpiderBite;
import com.CPS240Final.Plugin.Disease.Zombification;
import com.CPS240Final.Plugin.Util.Util;

/**
 * Handles getting diseases from environmental factors
 * @author Johnny Leek
 *
 */
public class DiseaseListener implements Listener {

	//Chances to get disease (1 in <VARIABLE> chance)
	private final int ZOMBIE_DISEASE_CHANCE = Main.getInstance().getConfig().getInt("Disease.DiseaseChance.Zombie");
	private final int SPIDER_DISEASE_CHANCE = Main.getInstance().getConfig().getInt("Disease.DiseaseChance.Spider");
	private final int COVID_DISEASE_CHANCE = Main.getInstance().getConfig().getInt("Disease.DiseaseChance.COVID-19");

	/**
	 * When a player is hit
	 * @param e - the event
	 */
	@EventHandler
	public void onPlayerHit(EntityDamageByEntityEvent e) {

		if(!(e.getEntity() instanceof Player)) return;

		Player player = (Player) e.getEntity();

		if(e.getDamager() instanceof Zombie) {
			boolean willGetDisease = (Util.randomNum(0, ZOMBIE_DISEASE_CHANCE) == 1);
			if(willGetDisease)
				Util.addDisease(player, new Zombification(player), DiseaseCause.ENEMY);
			else
				return;
		}

		else if(e.getDamager() instanceof Spider) {
			boolean willGetDisease = (Util.randomNum(0, SPIDER_DISEASE_CHANCE) == 1);
			if(willGetDisease)
				Util.addDisease(player, new SpiderBite(player), DiseaseCause.ENEMY);
			else
				return;
		}


	}

	/**
	 * @author Owen Smith
	 * @param event - player consume item event
	 * Gives player chance of contracting disease
	 * when eating certain foods
	 */
	@EventHandler
	public void onEatEvent(PlayerItemConsumeEvent event) {
		 Player player = event.getPlayer();
		 ItemStack item = event.getItem();

		 Material kelp = Material.DRIED_KELP;
		 if (item.getType() == kelp ) {
			 boolean willGetDisease = (Util.randomNum(0, COVID_DISEASE_CHANCE) == 1);
			 if (willGetDisease) {
				 Util.addDisease(player, new CoronaVirus(player), DiseaseCause.FOOD);
			 }
			 else
				 return;
		 }
	}

	/**
	 * @author Owen Smith
	 * @param event - entity death event
	 *
	 * When bats die, they drop custom
	 * "Bat Meat" for use in covid-19
	 * implementation.
	 */
	@EventHandler
	public void entityDeathEvent(EntityDeathEvent event) {
		LivingEntity entity = event.getEntity();


		if (entity instanceof Bat) {
			entity.getLocation().getWorld().dropItem(entity.getLocation(), Util.createItem(Material.DRIED_KELP,ChatColor.RED + "Bat Meat", "Bats drop meat now? It's probably poisonous or something..."));
			event.getDrops().clear();
		}


	}

	/**
	 * When a player dies
	 * @param event - the event
	 */
	@EventHandler
	public void playerDeathEvent(PlayerDeathEvent event) {
		String playerDiseases = (String)Main.getInstance().getDatabase("users").getPlayerData(event.getEntity(), "diseases");

		if(playerDiseases != null) {
			String[] playerDiseasesArr = playerDiseases.split(",");
			for(Disease disease : Main.getInstance().getPossibleDiseases()) {

				for(int i = 0; i < playerDiseasesArr.length; i++) {
					if(disease.getName().contentEquals(playerDiseasesArr[i])) {

						if(event.getEntity().getLastDamageCause().getCause() == DamageCause.MAGIC)
							event.setDeathMessage(String.format("%s %s %s", Util.DISEASE_DEATH_PREFIX, event.getEntity().getDisplayName(), disease.getDeathMessage()));

						Util.removeDisease(event.getEntity(), disease);
					}
				}

			}

		}

	}

	/**
	 * When a player drinks medicine
	 * @param e - the event
	 */
	@EventHandler
	public void playerDrink(PlayerItemConsumeEvent e) {
		if(e.getItem().getItemMeta().getDisplayName().contains(Util.MEDICINE_NAME)) {
			Disease diseaseToCure = Util.getDisease(e.getItem().getItemMeta().getLore().get(2));
			if(diseaseToCure != null)
				Util.removeDisease(e.getPlayer(), diseaseToCure);
		}

	}

}
