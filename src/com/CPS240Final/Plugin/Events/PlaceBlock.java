package com.CPS240Final.Plugin.Events;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import com.CPS240Final.Plugin.Disease.Pharmacist;
import com.CPS240Final.Plugin.Util.Util;

import net.md_5.bungee.api.ChatColor;

/**
 * Events for when a user places a block
 * @author Johnny Leek
 *
 */
public class PlaceBlock implements Listener{
	
	/**
	 * When the player places the pharmacist summon campfire
	 * @param e - the event
	 */
	@EventHandler
	public void onPharmacistSummonPlace(PlayerInteractEvent e) {
		
		//If pharmacist summon placed
		try {
			if(e.getItem().equals(Util.PRESCRIPTION)) {
				
				Bukkit.getWorld("world").playSound(e.getPlayer().getLocation(), Sound.ENTITY_WITHER_AMBIENT, 1, 1);
				Bukkit.getWorld("world").strikeLightningEffect(e.getPlayer().getLocation());
				e.getPlayer().sendMessage(Util.DISEASE_PREFIX + "A travelling pharmacist has heard your pleas, and has come to assist you.");
				
				Pharmacist pharmacist = new Pharmacist();
				pharmacist.spawnEntity(e.getPlayer().getLocation());
				e.getPlayer().getInventory().remove(Util.PRESCRIPTION);
				
			}
		} catch(NullPointerException ex) {
			return;
		}
		
	}
	
	
	
}
