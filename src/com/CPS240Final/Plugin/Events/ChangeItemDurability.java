package com.CPS240Final.Plugin.Events;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Changes all crafted items to 1/2 durability
 * @author Johnny Leek
 *
 */
public class ChangeItemDurability implements Listener {

	@EventHandler
	public void onCraftItem(PrepareItemCraftEvent e) {
		try {
			//Gets the result of the crafting recipe
			ItemStack craftedItem = e.getRecipe().getResult();
			//Gets the item meta of the item
			ItemMeta craftedItemMeta = craftedItem.getItemMeta();

			//If the item has durability
			if(craftedItemMeta instanceof Damageable) {
				//Get the damageable meta attributes
				Damageable damageItemMeta = (Damageable) craftedItemMeta;

				//Sets the item to half durability
				damageItemMeta.setDamage(craftedItem.getType().getMaxDurability() / 2);
				craftedItem.setItemMeta((ItemMeta)damageItemMeta);
			}


			e.getInventory().setResult(craftedItem);
		} catch(NullPointerException npe) {
			return;
		}
	}
}
