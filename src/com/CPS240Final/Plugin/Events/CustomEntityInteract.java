package com.CPS240Final.Plugin.Events;

import java.util.Map;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.CustomEntities.CustomEntity;
import com.CPS240Final.Plugin.CustomEntities.CustomEntityType;

/**
 * Handles interaction with custom entity
 * @author Johnny Leek
 *
 */
public class CustomEntityInteract implements Listener {

	@EventHandler
	public void playerInteractEntity(PlayerInteractEntityEvent e) {
		
		Player p = e.getPlayer();
		Entity entity = e.getRightClicked();
		
		if(!e.getHand().equals(EquipmentSlot.HAND)) 
			return;
		
		for(Map.Entry<CustomEntity, CustomEntityType> customEntity : Main.getInstance().getCustomEntities().entrySet()) {
			
			if(customEntity.getKey().getEntity() == entity) {
				e.setCancelled(true);
				customEntity.getKey().onInteract(p);
				return;
			}
			
		}
	}
	
}
