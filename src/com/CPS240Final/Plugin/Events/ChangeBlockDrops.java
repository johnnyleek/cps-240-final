package com.CPS240Final.Plugin.Events;


import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.CPS240Final.Plugin.Main;

/**
 * Changes the drop rate of ores along with not allowing blocks to be broken without tools
 * @author jeremyproksch
 *
 */
public class ChangeBlockDrops implements Listener {
	
	//list of ores in Minecraft
	private Material[] ores = new Material[] {Material.COAL_ORE, Material.DIAMOND_ORE, Material.EMERALD_ORE,
			Material.GOLD_ORE, Material.IRON_ORE, Material.LAPIS_ORE, Material.REDSTONE_ORE
	};
	
	//create new array with corresponding 'drop rates' modifiable in configs
	private double[] dropRates = new double[] {Main.getInstance().getConfig().getDouble("Ores.DropRates.Coal"), 
			Main.getInstance().getConfig().getDouble("Ores.DropRates.Diamond"), Main.getInstance().getConfig().getDouble("Ores.DropRates.Emerald"),
			Main.getInstance().getConfig().getDouble("Ores.DropRates.Gold"), Main.getInstance().getConfig().getDouble("Ores.DropRates.Iron"),
			Main.getInstance().getConfig().getDouble("Ores.DropRates.Lapis"), Main.getInstance().getConfig().getDouble("Ores.DropRates.Redstone")};
	
	//list of items breakable by hand
	private Material[] breakableByHand = new Material[] {Material.RED_SAND, Material.SAND, Material.GRAVEL,
			Material.DIRT, Material.COARSE_DIRT
	};
	
	//list of useable tools
	private Material[] tools = new Material[] {Material.DIAMOND_SHOVEL, Material.GOLDEN_SHOVEL, Material.IRON_SHOVEL,
			Material.STONE_SHOVEL, Material.DIAMOND_PICKAXE, Material.IRON_PICKAXE,
			Material.GOLDEN_PICKAXE, Material.STONE_PICKAXE, Material.DIAMOND_AXE,
			Material.GOLDEN_AXE, Material.IRON_AXE, Material.STONE_AXE
	};

	/**
	 * Detects a block being broken and then checks if the block is allowed to be broken,
	 * and changes the blocks dropped xp and drops accordingly
	 * @param event - the blocking being broken event
	 */
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)
	{
		boolean canBreak = false;
		//gets the material type of the block broken
		Material block = event.getBlock().getBlockData().getMaterial();
		
		//gets the tool used to break the block
		ItemStack tool = event.getPlayer().getInventory().getItemInMainHand();
		
		//checks if the item used to break the block is a useable tool
		for(Material item: tools)
		{
			if(tool.getType().equals(item))
			{
				canBreak = true;
			}
		}
		//cancels break event if using hand unless material is sand, dirt, gravel,
		if(tool.getType().equals(Material.AIR))
		{
			
			for(Material item: breakableByHand)
			{
				if(block.equals(item))
				{
					canBreak = true;
				}
			}
		}
		//cancels break event if a tool isn't being used unless block being broken is allowed
		if(canBreak == false)
		{
			event.setCancelled(true);
		}
		
		//sets the dropped exp to 0 for any broken block
		event.setExpToDrop(0);
		
		//lowers the drop rate of ores
		int i = 0;
		for(Material ore : ores)
		{
			if(block.equals(ore))
			{
				//if the random number is higher than the drop rate the ore does not drop anything
				if(Math.random() > dropRates[i])
				{
					event.setDropItems(false);
				}
			}
			i++;
		}
	}
}


