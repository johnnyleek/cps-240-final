package com.CPS240Final.Plugin.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import com.CPS240Final.Plugin.Main;
import com.CPS240Final.Plugin.Database.Database;
import com.CPS240Final.Plugin.Disease.Disease;
import com.CPS240Final.Plugin.Disease.DiseaseCause;

/**
 * Utility methods that are used in multiple places
 * @author Johnny Leek
 *
 */
public class Util {

	public static final String DISEASE_PREFIX = ChatColor.RED + "" + ChatColor.BOLD + "[DISEASE] " + 
											    ChatColor.RESET + "" +
											    ChatColor.YELLOW;
	
	public static final String DISEASE_DEATH_PREFIX = ChatColor.RED + "" + ChatColor.BOLD + "[DEATH] " + 
													  ChatColor.RESET + "" +
													  ChatColor.YELLOW;
	
	public static final String MEDICINE_NAME = ChatColor.GREEN + "MEDICINE";
	
	public static final String CARRY_WEIGHT_PREFIX = ChatColor.BLUE.toString() + ChatColor.BOLD + "[CARRY WEIGHT]" +
													 ChatColor.RESET + "" + 
													 ChatColor.YELLOW + " ";
	
	public static final ItemStack HYDRO_FLASK = createPotion(ChatColor.YELLOW.toString() + ChatColor.MAGIC.toString() + ChatColor.BOLD + "...." + ChatColor.RESET + ChatColor.RED.toString() + ChatColor.BOLD + "  H" + ChatColor.YELLOW.toString() + ChatColor.BOLD + "Y" + ChatColor.GREEN.toString() + ChatColor.BOLD + "D" + ChatColor.AQUA.toString() + ChatColor.BOLD + "R" + ChatColor.BLUE.toString() + ChatColor.BOLD + "O" + ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "F" + ChatColor.RED.toString() + ChatColor.BOLD + "L" + ChatColor.YELLOW.toString() + ChatColor.BOLD + "A" + ChatColor.GREEN.toString() + ChatColor.BOLD + "S" + ChatColor.AQUA.toString() + ChatColor.BOLD + "K  " + ChatColor.YELLOW.toString() + ChatColor.MAGIC.toString() + ChatColor.BOLD + "....", Color.fromRGB(253, 253, 150), PotionType.AWKWARD, ChatColor.RED + "H" + ChatColor.YELLOW + "y" + ChatColor.GREEN + "d" + ChatColor.AQUA + "r" + ChatColor.BLUE + "o" + ChatColor.LIGHT_PURPLE + " " + ChatColor.RED + "F" + ChatColor.YELLOW + "l" + ChatColor.GREEN + "a" + ChatColor.AQUA + "s" + ChatColor.BLUE + "k" + ChatColor.LIGHT_PURPLE + "s" + ChatColor.RED + "," + ChatColor.YELLOW + " " + ChatColor.GREEN + "s" + ChatColor.AQUA + "e" + ChatColor.BLUE + "r" + ChatColor.LIGHT_PURPLE + "v" + ChatColor.RED + "i" + ChatColor.YELLOW + "n" + ChatColor.GREEN + "g" + ChatColor.AQUA + " " + ChatColor.BLUE + "t" + ChatColor.LIGHT_PURPLE + "h" + ChatColor.RED + "e"  , ChatColor.YELLOW + "m" + ChatColor.GREEN + "o" + ChatColor.AQUA + "s" + ChatColor.BLUE + "t" + ChatColor.LIGHT_PURPLE + " " + ChatColor.RED + "b" + ChatColor.YELLOW + "a" + ChatColor.GREEN + "s" + ChatColor.AQUA + "i" + ChatColor.BLUE + "c" + ChatColor.LIGHT_PURPLE + " " + ChatColor.RED + "o" + ChatColor.YELLOW + "f" + ChatColor.GREEN + " " + ChatColor.AQUA + "w" + ChatColor.BLUE + "h" + ChatColor.LIGHT_PURPLE + "i" + ChatColor.RED + "t" + ChatColor.YELLOW + "e" + ChatColor.GREEN + " " + ChatColor.AQUA + "g" + ChatColor.BLUE + "i" + ChatColor.LIGHT_PURPLE + "r" + ChatColor.RED + "l" + ChatColor.YELLOW + "s" + ChatColor.GREEN + " " + ChatColor.AQUA + "a" + ChatColor.BLUE + " " + ChatColor.LIGHT_PURPLE + "s" + ChatColor.RED + "o" + ChatColor.YELLOW + "l" + ChatColor.GREEN + "u" + ChatColor.AQUA + "t" + ChatColor.BLUE + "i" + ChatColor.LIGHT_PURPLE + "o" + ChatColor.RED + "n" + ChatColor.YELLOW + " "  , ChatColor.GREEN + "t" + ChatColor.AQUA + "o" + ChatColor.BLUE + " " + ChatColor.LIGHT_PURPLE + "t" + ChatColor.RED + "h" + ChatColor.YELLOW + "e" + ChatColor.GREEN + "i" + ChatColor.AQUA + "r" + ChatColor.BLUE + " " + ChatColor.LIGHT_PURPLE + "m" + ChatColor.RED + "o" + ChatColor.YELLOW + "s" + ChatColor.GREEN + "t" + ChatColor.AQUA + " " + ChatColor.BLUE + "b" + ChatColor.LIGHT_PURPLE + "a" + ChatColor.RED + "s" + ChatColor.YELLOW + "i" + ChatColor.GREEN + "c" + ChatColor.AQUA + " " + ChatColor.BLUE + "n" + ChatColor.LIGHT_PURPLE + "e" + ChatColor.RED + "e" + ChatColor.YELLOW + "d" + ChatColor.GREEN + "!" , ChatColor.AQUA + "#" + ChatColor.BLUE + "H" + ChatColor.LIGHT_PURPLE + "y" + ChatColor.RED + "d" + ChatColor.YELLOW + "r" + ChatColor.GREEN + "a" + ChatColor.AQUA + "t" + ChatColor.BLUE + "e" + ChatColor.LIGHT_PURPLE + "O" + ChatColor.RED + "r" + ChatColor.YELLOW + "D" + ChatColor.GREEN + "i" + ChatColor.AQUA + "e" + ChatColor.BLUE + "d" + ChatColor.LIGHT_PURPLE + "r" + ChatColor.RED + "a" + ChatColor.YELLOW + "t" + ChatColor.GREEN + "e");
	
	public static final ItemStack RESPIRATOR = createItem(Material.IRON_CHESTPLATE, ChatColor.GREEN + "RESPIRATOR", ChatColor.RED + "Increases your breath capacity");
	
	public static final ItemStack PRESCRIPTION = createItem(Material.PAPER, ChatColor.AQUA.toString() + ChatColor.BOLD + "Prescription", ChatColor.GREEN + "Summons a pharmacist to sell you medicine!");
	
	/**
	 * Creates a new custom item
	 * @param material - The item to create
	 * @param name - A custom name for the item
	 * @param description - A custom description for the item
	 * @return - The custom item
	 */
	public static ItemStack createItem(Material material, String name, String... description) {
		ItemStack item = new ItemStack(material, 1);
		ItemMeta itemMeta = item.getItemMeta();
		
		itemMeta.setDisplayName(name);
		itemMeta.setLore(Arrays.asList(description));
		
		item.setItemMeta(itemMeta);
		
		return item;
	}
	
	
	/**
	 * Creates a new custom potion
	 * @param name - A custom name for the potion
	 * @param color - The color of the potion
	 * @param effect - The potion effect to give
	 * @param description - The description of the potion
	 * @return - The custom potion
	 */
	public static ItemStack createPotion(String name, Color color, PotionType effect, String...description) {
		ItemStack potion = new ItemStack(Material.POTION, 1);
		
		PotionMeta potionMeta = (PotionMeta) potion.getItemMeta();
		potionMeta.setDisplayName(name);
		potionMeta.setLore(Arrays.asList(description));
		potionMeta.setColor(color);
		
		if(effect != null) potionMeta.setBasePotionData(new PotionData(effect));
		
		potion.setItemMeta(potionMeta);
		
		return potion;
	}
	
	/**
	 * Returns a string array of the current players diseases
	 * @param player - the player of which to get diseases
	 * @return a string array of all of the players diseases
	 */
	public static String[] getDiseases(Player player) {
		
		Database userDatabase = Main.getInstance().getDatabase("users");
		String diseases = userDatabase.getPlayerData(player, "diseases");
		if(diseases == null) return new String[] {""};
		else return userDatabase.getPlayerData(player, "diseases").toString().split(",");
		
	}
	
	/**
	 * Saves the users current diseases to the database
	 * @param player - the player to save data to
	 * @param diseases - a comma separated string of all the users diseases
	 */
	public static void saveDiseases(Player player, String diseases) {
		if(diseases == null)
			Main.getInstance().getDatabase("users")
			.setPlayerData(player, new String[] {"diseases"}, new String[] {null});
		else
			Main.getInstance().getDatabase("users")
			.setPlayerData(player, new String[] {"diseases"}, new String[] {String.format("'%s'", diseases)});
	}
	
	/**
	 * Adds the specified disease to the specified player
	 * @param player - the player to give disease to
	 * @param disease - the disease to add
	 */
	public static void addDisease(Player player, Disease disease, DiseaseCause cause) {
		
		String[] diseases = getDiseases(player);
		ArrayList<String> userDiseases;
		if(diseases[0].equals("")) userDiseases = new ArrayList<String>();
		else userDiseases = new ArrayList<String>(Arrays.asList(diseases));
		
		if(userDiseases.contains(disease.getName()) && cause == DiseaseCause.COMMAND) {
			player.sendMessage(DISEASE_PREFIX + ChatColor.GREEN + "You already have that disease!");
			return;
		}
		
		userDiseases.add(disease.getName());
		
		String newDiseases = String.join(",", userDiseases);
		
		saveDiseases(player, newDiseases);
		
		for(PotionEffect diseaseEffect : disease.getEffects()) {
			player.addPotionEffect(diseaseEffect);
		}
		
		player.sendMessage(DISEASE_PREFIX + "You have contracted \"" + disease.getName() + ChatColor.YELLOW + "\"");
		
		
	}
	
	/**
	 * Removes the specified disease from the specified player
	 * @param player - the player to remove disease from
	 * @param disease - the disease to remove
	 */
	public static void removeDisease(Player player, Disease disease) {
		
		String[] diseases = getDiseases(player);
		if(diseases[0].equals("")) {
			player.sendMessage(DISEASE_PREFIX + ChatColor.GREEN + "You don't have any diseases!");
			return;
		}
		
		ArrayList<String> userDiseases = new ArrayList<String>(Arrays.asList(diseases));
		
		if(!userDiseases.contains(disease.getName())) {
			player.sendMessage(DISEASE_PREFIX + ChatColor.GREEN + "You don't have that disease!");
			return;
		}
		
		userDiseases.remove(disease.getName());
		
		String newDiseases;
		if(userDiseases.size() == 0) newDiseases = null;
		else newDiseases = String.join(",", userDiseases);
		
		saveDiseases(player, newDiseases);
		
		for(PotionEffect diseaseEffect : disease.getEffects()) {
			player.removePotionEffect(diseaseEffect.getType());
		}
		
		player.sendMessage(DISEASE_PREFIX + ChatColor.GREEN + "You have been cured of \"" + disease.getName() + ChatColor.GREEN + "\"!");
		
		resetDiseaseEffects(player);
		
	}
	
	/**
	 * Adds all potion effects that a player should have
	 * This is necessary because if two diseases have the same effect
	 * and one of the diseases gets cured, they will no longer have that
	 * effect, even though the other disease possesses that effect also
	 * @param player - The player to reset effects for
	 */
	public static void resetDiseaseEffects(Player player) {
		String[] diseases = getDiseases(player);
		if(diseases[0].equals(""))
			return;
		
		for(String diseaseName : diseases) {
			for(Disease disease : Main.getInstance().getPossibleDiseases()) {
				if(diseaseName.equalsIgnoreCase(disease.getName())) {
					
					for(PotionEffect diseaseEffect : disease.getEffects()) {
						player.addPotionEffect(diseaseEffect);
					}
					
				}
			}
		}
	}
	
	/**
	 * Checks if the user has the given disease
	 * @param player - the player to check
	 * @param disease - the disease to check for
	 * @return true/false for user having disease
	 */
	public static boolean hasDisease(Player player, Disease disease) {
		String[] diseases = getDiseases(player);
		if(diseases[0].equals(""))
			return false;
		
		ArrayList<String> userDiseases = new ArrayList<String>(Arrays.asList(diseases));
		
		if(userDiseases.contains(disease.getName()))
			return true;
		else
			return false;
	}
	
	/**
	 * Returns a disease object based on the name of the disease
	 * @param name - the name of the disease to get
	 * @return - the disease with the provided name
	 */
	public static Disease getDisease(String name) {
		for(Disease disease : Main.getInstance().getPossibleDiseases()) {
			if(disease.getName().equalsIgnoreCase(name) || disease.getCmdName().equalsIgnoreCase(name)) {
				return disease;
			}
		}
		return null;
	}
	
	/**
	 * Generates random number between bounds
	 * @param min - minimum number (inclusive)
	 * @param max - maximum number (exclusive)
	 * @return random number between min and max
	 * https://mkyong.com/java/java-generate-random-integers-in-a-range/
	 */
	public static int randomNum(int min, int max) {
		Random random = new Random();
		return random.nextInt((max - min) + 1) + min;
	}
	
}
