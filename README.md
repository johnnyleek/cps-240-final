# CPS 240 Final

This project is a Minecraft server plugin that creates an "Ultra Hardcore" gamemode
into Minecraft (the regular hardcore mode wasn't enough). This runs serverside
as opposed to clientside to allow for ease of use (the client doesn't have to
download anything). To play this gamemode, one can simply connect to `gary.owo.asia`
and start playing right away!

However, to contribute to the code/run this locally, follow the steps below.

# Installation/Setup

#### Prerequisites
*  Have a running Spigot Minecraft server ([see this link for instructions](https://www.spigotmc.org/wiki/spigot-installation/))
*  Have the Spigot API (Version 1.15.2) jar downloaded on your machine ([download page](https://getbukkit.org/download/spigot))

### Setup (Eclipse)
1.  Clone this repository somewhere on your computer
2.  Open Eclipse
3.  `File > Open Projects From File System`
4.  `Directory` (Next to Import Source)
5.  Find and select the cloned repository
6.  `Finish`
7.  Right click on `cps-240-final` in the Package Explorer
8.  `Build Path > Configure Build Path`
9.  `Libraries`
10.  Click on `Classpath` in the main menu then click the `Add External JARs...` button on the right
11. Find and select the Spigot API JAR (not the server JAR)
12. `Apply and Close`

### Export and run code
1.  Right click on `cps-240-final`
2.  `Export`
3.  `Java > JAR file`
4.  `Next`
5.  Ensure only the `cps-240-final` project is selected
6.  Set the JAR file export destination to `Server directory > plugins`
7.  `Finish`
8.  Run the server or type `/reload`
